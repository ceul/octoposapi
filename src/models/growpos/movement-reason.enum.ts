export enum MovementReasonEnum {
    // IN values
        IN_PURCHASE = 1,                          //Supplier Purchase
        IN_REFUND = 2,                            //Customer Refund
        IN_MOVEMENT = 4,                          //Adjust Add
    // OUT values   
        OUT_SALE = -1,                             //Sale
        OUT_REFUND = -2,                           //Supplier Return
        OUT_BREAK = -3,                            //Breakage
        OUT_MOVEMENT = -4,                         //Adjust Subtract
        OUT_SAMPLE = -5,                           //Given Sample
        OUT_FREE = -6,                             //Given Free 
        OUT_USED = -7,                             //Used item   
        OUT_SUBTRACT = -8,                         //Rectify error per JM requirement        
    // TRANSFER
        OUT_CROSSING = 1000,
}