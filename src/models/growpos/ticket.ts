import { CustomerGrowPos } from "./customer";
import { PeopleGrowPos } from "./people";
import { PaymentGrowPos } from "./payment";
import { TicketLineGrowPos } from "./ticket-line";
import { TaxLineGrowPos } from "./tax-line";
import { ContractGrowPos } from "./contract";

export class TicketGrowPos {
    public host: string;
    public id: string; // Es el identificador unico del ticket uui
    public ticketType: number;
    public ticketId: number; // Es el autoincremental del ticket
    public pickupId: number;
    public date: Date;
    public contract: ContractGrowPos;
   // public Properties attributes;
    public user: PeopleGrowPos;
    public multiply: number;
    public customer: CustomerGrowPos;
    public activeCash: string;
    public lines: TicketLineGrowPos[];
    public payments: PaymentGrowPos[];
    public taxes: TaxLineGrowPos[];
    public response: string;
    public loyaltyCardNumber: string;
    public oldTicket: boolean;
    public tip: boolean;
    public isProcessed: boolean;
    public locked: string;
    public nsum: number;
    public ticketstatus: number;
    public note: string;
    public total: number;
    public isRefund: boolean;
  //  public Object size;
}