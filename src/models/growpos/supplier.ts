export class SupplierGrowPos {
    public id: string;
    public searchkey: string;
    public taxid: string;
    public name: string;
    public maxdebt: number;
    public address: string;
    public address2: string;
    public postal: string;
    public city: string;
    public region: string;
    public country: string;
    public firstname: string;
    public lastname: string;
    public email: string;
    public phone: string;
    public phone2: string;
    public fax: string;
    public notes: string;
    public visible: boolean;
    public curdate: Date;
    public curdebt: number;
    public vatid: string;
}