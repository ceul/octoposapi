import { PlaceGrowPos } from "./place";

export class FloorGrowPos {
    public id: string;
    public name: string;
    public image: string;
    public type: string;
    public places: PlaceGrowPos[];
}