
export class CategoryGrowPos {
    id: string;
    name: string;
    parentid: string;
    image: any;
    texttip: string;
    catshowname: number;
    catorder: string;
    type: string
}