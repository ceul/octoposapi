export class AttributeInstanceGrowPos{
    public id: string;
    public attributesetinstance_id: string;
    public attribute_id: string;
    public value: string;
}