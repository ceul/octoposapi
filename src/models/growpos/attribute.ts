import { AttributeValueGrowPos } from "./attribute-value";

export class AttributeGrowPos {
    id: string;
    name: string;
    attribute_values: AttributeValueGrowPos[]
}