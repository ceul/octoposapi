import { OrderStateEnum } from "./order-state.enum";
import { OrderTypeEnum } from "./order-type.enum";
import { OrderProductsGrowPos } from "./order-products";

export class ExternalPlatformGrowPos{
    public id: number;
    public name: string;
    public version: string;
    public api_key: string;
    public token: string;
    public url: string;
}