export class CreditNoteGrowPos {
    public id: number;
    public receipt: string;
    public date: Date;
    public note: string;
    public state: boolean;
}