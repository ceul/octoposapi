import { TaxGrowPos } from "./tax";
import { ProductGrowPos } from "./product";
import { AttributeInstanceGrowPos } from "./attribute-instance";

export class TicketLineGrowPos {
    public ticket: string;
    public line: number;
    public multiply: number;
    public price: number;
    public tax: TaxGrowPos;
    public attributes: ProductGrowPos;
    public productid: string;
    public attsetinstid: string;
    public updated: boolean;
    public attributeinstance: AttributeInstanceGrowPos[];
}