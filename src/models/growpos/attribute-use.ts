import { AttributeGrowPos } from "./attribute";

export default class AttributeUseGrowPos {
    id: string;
    attributeset_id: string;
    attribute: AttributeGrowPos;
    line: number;
}