export enum FilterOperator {
    None = 1,
    Equals,
    Distinct,
    Greater,
    Less,
    GreaterOrEqual,
    LessOrEqual
}