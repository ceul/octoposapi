import { ProductGrowPos } from "./product";
import { AttributeInstanceGrowPos } from "./attribute-instance";

export class StockDiaryGrowPos {
    public id: string;
    public datenew: Date;
    public reason: number;
    public location: string;
    public product: string;
    public attributesetinstance_id: string;
    public units: number;
    public price: number;
    public appuser: string;
    public supplier: string;
    public supplierdoc: string;
    public productObject: ProductGrowPos;
    public attributeinstance: AttributeInstanceGrowPos[];
}