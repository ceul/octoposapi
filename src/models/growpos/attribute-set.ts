import { AttributeGrowPos } from "./attribute";

export class AttributeSetGrowPos {
    id: string;
    name: string;
    attributes: AttributeGrowPos[]
}