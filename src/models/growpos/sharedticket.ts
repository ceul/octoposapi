import { ProductCatGrowPos } from "./product-cat";
import { PeopleGrowPos } from "./people";


export class SharedTicketGrowPos {
    id: string;
    name: string;
    products: ProductCatGrowPos[];
    appuser: PeopleGrowPos;
    pickupid: number;
    content: Buffer;
}