export class PermissionsGrowPos {
    public id_permissions: string;
    public father: string;
    public level: string;
    public name: string;
    public icon: string;
    public url: string;
}