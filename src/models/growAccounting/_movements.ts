import { CustomerGrowPos } from '../growpos/customer';

export class MovementsGrowAccounting{
    public id_movement: string;
    public id_third_party: CustomerGrowPos;
    public date: string;
    public description: string;
    public state: string;
}