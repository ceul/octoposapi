import { AttributesGrowAccounting } from './_attributes';
import { AccountsGrowAccounting } from './_accounts';

export class AttributesAccountsGrowAccounting{
    public id_account: AccountsGrowAccounting;
    public id_attribute: AttributesGrowAccounting;
}