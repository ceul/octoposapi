import { AccountsGrowAccounting } from './_accounts';

export class MovementsRowGrowAccounting{
    public id_movement_row: string;
    public id_movement: string;
    public id_account: AccountsGrowAccounting;
    public sign: string;
    public state: string;
}