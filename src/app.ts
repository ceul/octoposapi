//    GrowPos
//    Copyright (c) 2020 GrowPos
//    http://GrowPos.co
//
//    This file is part of GrowPos
//
//    GrowPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GrowPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GrowPos.  If not, see <http://www.gnu.org/licenses/>.
import * as express from "express";
import * as bodyParser from "body-parser";
import { MiscellaneousRoutes } from "./routes/miscellaneousRoutes";
import { DataBaseService } from './dataBaseService';
import { CustomerRoutes } from "./routes/customerRoutes";
import { CategoryRoutes } from "./routes/categoriesRoutes";
import { CharacteristicsRoutes } from "./routes/characteristicsRoutes";
import { ProductCharacteristicRoutes } from "./routes/productCharacteristicsRoutes";
import { PeopleRoutes } from "./routes/peopleRoutes";
import { RoleRoutes } from "./routes/roleRoutes";
import { ProductRoutes } from "./routes/productRoutes";
import { UnitMeasureRoutes } from "./routes/unitMeasureRoutes";
import { TaxCategoryRoutes } from "./routes/taxCategoryRoutes";
import { SupplierRoutes } from "./routes/supplierRoutes";
import { AttributeSetRoutes } from "./routes/attributeSetRoutes";
import { LocationRoutes } from "./routes/locationRoutes";
import { StockRoutes } from "./routes/stockRoutes";
import { SharedTicketRoutes } from "./routes/sharedTicketRoutes";
import { TicketRoutes } from "./routes/ticketRoutes";
import { TaxRoutes } from "./routes/taxRoutes";
import { CloseCashRoutes } from "./routes/closeCashRoutes";
import { FloorRoutes } from "./routes/floorRoutes";
import { PlaceRoutes } from "./routes/placeRoutes";
import { LineRemovedRoutes } from "./routes/lineRemovedRoutes";
import { DrawerOpenedRoutes } from "./routes/drawerOpenedRoutes";
import { PaymentRoutes } from "./routes/paymentRoutes";
import { ErrorHandler } from "./errorHandlerService";
import { LogRoutes } from "./routes/logRoutes";
import { ResourceRoutes } from "./routes/resourceRoutes";
import { AttributeRoutes } from "./routes/attribute";
import { ParkingMovRoutes } from './routes/parkingMovRoutes'
import { ParkingFeeRoutes } from "./routes/parkingFeeRoutes";
import { ContractRoutes } from "./routes/contractRoutes";
import { VehicleRoutes } from "./routes/vehicleRoutes";
import { OrderRoutes } from "./routes/orderRoutes";
import { AuthRoutes } from "./routes/authRoutes";
import { HttpRequestService } from "./httpRequestService";

// Grow Accounting
import { AccountTypesRoutes } from './routes/growAccounting/_account_types';
import { AccountsRoutes } from './routes/growAccounting/_accounts';
import { AccountBanksRoutes } from './routes/growAccounting/_accounts_banks';
import { AttributesRoutes } from './routes/growAccounting/_attributes';
import { AttributesAccountsRoutes } from './routes/growAccounting/_attributes_accounts';
import { BanksRoutes } from './routes/growAccounting/_banks';
import { BranchOfficesRoutes } from './routes/growAccounting/_branch_offices';
import { CostCentersRoutes } from './routes/growAccounting/_cost_centers';
import { MovementsAttributesRoutes } from './routes/growAccounting/_movement_attributes';
import { MovementsRowRoutes } from './routes/growAccounting/_movement_row';
import { MovementsRoutes } from './routes/growAccounting/_movements';
import { niifConceptsRoutes } from './routes/growAccounting/_niif_concepts';
import { ThirdPartyTypesRoutes } from './routes/third_party_typeRoutes';


class App {

    public app: express.Application;
    public miscellaneousRoutes: MiscellaneousRoutes = new MiscellaneousRoutes();
    public customerRoutes: CustomerRoutes = new CustomerRoutes();
    public categoryRoutes: CategoryRoutes = new CategoryRoutes();
    public characteristicsRoutes: CharacteristicsRoutes = new CharacteristicsRoutes();
    public productCharacteristicRoutes: ProductCharacteristicRoutes = new ProductCharacteristicRoutes();
    public peopleRoutes: PeopleRoutes = new PeopleRoutes();
    public roleRoutes: RoleRoutes = new RoleRoutes();
    public productRoutes: ProductRoutes = new ProductRoutes();
    public taxCategoryRoutes: TaxCategoryRoutes = new TaxCategoryRoutes();
    public unitMeasureRoutes: UnitMeasureRoutes = new UnitMeasureRoutes();
    public supplierRoutes: SupplierRoutes = new SupplierRoutes();
    public attributeSetRoutes: AttributeSetRoutes = new AttributeSetRoutes();
    public locationRoutes: LocationRoutes = new LocationRoutes();
    public stockRoutes: StockRoutes = new StockRoutes();
    public ticketRoutes: TicketRoutes = new TicketRoutes();
    public sharedTicketRoutes: SharedTicketRoutes = new SharedTicketRoutes();
    public taxRoutes: TaxRoutes = new TaxRoutes();
    public closeCashRoutes: CloseCashRoutes = new CloseCashRoutes();
    public floorRoutes: FloorRoutes = new FloorRoutes();
    public placeRoutes: PlaceRoutes = new PlaceRoutes();
    public lineRemovedRoutes: LineRemovedRoutes = new LineRemovedRoutes();
    public drawerOpenedRoutes: DrawerOpenedRoutes = new DrawerOpenedRoutes();
    public paymentRoutes: PaymentRoutes = new PaymentRoutes();
    public logRoutes: LogRoutes = new LogRoutes();
    public resourceRoutes: ResourceRoutes = new ResourceRoutes();
    public attributeRoutes: AttributeRoutes = new AttributeRoutes();
    public parkingMovRoutes: ParkingMovRoutes = new ParkingMovRoutes();
    public parkingFeeRoutes: ParkingFeeRoutes = new ParkingFeeRoutes();
    public contractRoutes: ContractRoutes = new ContractRoutes();
    public vehicleRoutes: VehicleRoutes = new VehicleRoutes();
    public orderRoutes: OrderRoutes = new OrderRoutes();
    public authRoutes: AuthRoutes = new AuthRoutes()
    public errorHandler: ErrorHandler = new ErrorHandler();

    public accountTypesRoutes: AccountTypesRoutes = new AccountTypesRoutes();
    public accountsRoutes: AccountsRoutes = new AccountsRoutes();
    public accountBanksRoutes: AccountBanksRoutes = new AccountBanksRoutes();
    public attributesRoutes: AttributesRoutes = new AttributesRoutes();
    public attributesAccountsRoutes: AttributesAccountsRoutes = new AttributesAccountsRoutes();
    public banksRoutes: BanksRoutes = new BanksRoutes();
    public branchOfficesRoutes: BranchOfficesRoutes = new BranchOfficesRoutes();
    public costCentersRoutes: CostCentersRoutes= new CostCentersRoutes();
    public movementsAttributesRoutes: MovementsAttributesRoutes = new MovementsAttributesRoutes();
    public movementsRowRoutes: MovementsRowRoutes = new MovementsRowRoutes();
    public movementsRoutes: MovementsRoutes = new MovementsRoutes();
    public niifConceptsRoutes: niifConceptsRoutes = new niifConceptsRoutes();
    public thirdPartyTypesRoutes: ThirdPartyTypesRoutes = new ThirdPartyTypesRoutes();

    private connection;
    private httpRequestService

    constructor() {
        this.app = express();
        this.config();
        this.miscellaneousRoutes.routes(this.app);
        this.customerRoutes.routes(this.app);
        this.categoryRoutes.routes(this.app);
        this.characteristicsRoutes.routes(this.app);
        this.productCharacteristicRoutes.routes(this.app);
        this.peopleRoutes.routes(this.app);
        this.roleRoutes.routes(this.app);
        this.productRoutes.routes(this.app);
        this.taxCategoryRoutes.routes(this.app);
        this.unitMeasureRoutes.routes(this.app);
        this.supplierRoutes.routes(this.app);
        this.attributeSetRoutes.routes(this.app);
        this.locationRoutes.routes(this.app);
        this.stockRoutes.routes(this.app);
        this.ticketRoutes.routes(this.app);
        this.sharedTicketRoutes.routes(this.app);
        this.taxRoutes.routes(this.app);
        this.closeCashRoutes.routes(this.app);
        this.floorRoutes.routes(this.app);
        this.placeRoutes.routes(this.app);
        this.lineRemovedRoutes.routes(this.app);
        this.drawerOpenedRoutes.routes(this.app);
        this.paymentRoutes.routes(this.app);
        this.logRoutes.routes(this.app);
        this.resourceRoutes.routes(this.app);
        this.attributeRoutes.routes(this.app)
        this.parkingMovRoutes.routes(this.app);
        this.parkingFeeRoutes.routes(this.app);
        this.contractRoutes.routes(this.app);
        this.vehicleRoutes.routes(this.app);
        this.authRoutes.routes(this.app)
        this.orderRoutes.routes(this.app)
        this.accountTypesRoutes.routes(this.app);
        this.accountsRoutes.routes(this.app);
        this.accountBanksRoutes.routes(this.app);
        this.attributesRoutes.routes(this.app);
        this.attributesAccountsRoutes.routes(this.app);
        this.banksRoutes.routes(this.app);
        this.branchOfficesRoutes.routes(this.app);
        this.costCentersRoutes.routes(this.app);
        this.movementsAttributesRoutes.routes(this.app);
        this.movementsRowRoutes.routes(this.app);
        this.movementsRoutes.routes(this.app);
        this.niifConceptsRoutes.routes(this.app);
        this.thirdPartyTypesRoutes.routes(this.app);
        this.errorHandler.routes(this.app);

        this.connection = DataBaseService.getInstance();
        this.httpRequestService = HttpRequestService.getInstance();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
}

export default new App().app;