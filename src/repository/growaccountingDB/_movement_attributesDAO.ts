//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { MovementsAttributesGrowAccounting } from '../../models/growAccounting/_movement_attributes';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class MovementsAttributesDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertMovementsAttributes(data: MovementsAttributesGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _movement_attributes SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsAttributesDAOGrowAccounting.name} -> ${this.insertMovementsAttributes.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAMovementsAttributes() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_movement_attributes,
                    id_movement_row,
                    id_attribute,
                    note
                    FROM _movement_attributes;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsAttributesDAOGrowAccounting.name} -> ${this.getAMovementsAttributes.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getMovementsAttributesById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_movement_attributes,
                    id_movement_row,
                    id_attribute,
                    note
                    FROM _movement_attributes
                    WHERE id_movement_attributes = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsAttributesDAOGrowAccounting.name} -> ${this.getMovementsAttributesById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteMovementsAttributes(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _movement_attributes 
                    WHERE id_movement_attributes = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsAttributesDAOGrowAccounting.name} -> ${this.deleteMovementsAttributes.name}: ${error}`)
            throw new Error(error)
        }
    }
}