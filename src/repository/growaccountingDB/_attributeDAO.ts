//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { AttributesGrowAccounting } from '../../models/growAccounting/_attributes';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class AttributesDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAttribute(data: AttributesGrowAccounting, con = null) {
        try {
            let id = uuid.v4();
            data.id_attribute = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _attributes SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesDAOGrowAccounting.name} -> ${this.insertAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributes() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_attribute,
                    description,
                    type
                    FROM _attributes;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesDAOGrowAccounting.name} -> ${this.getAttributes.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributesById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_attribute,
                    description,
                    type
                    FROM _attributes
                    WHERE id_attribute = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesDAOGrowAccounting.name} -> ${this.getAttributesById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAttribute(data: AttributesGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _attributes SET
                        id_attribute = ? ,
                        description = ?,
                        type = ?
                        WHERE id_attribute = ?;`,
                    [data.id_attribute,
                    data.description,
                    data.type,
                    data.id_attribute]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesDAOGrowAccounting.name} -> ${this.updateAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttribute(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _attributes 
                    WHERE id_attribute = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributesDAOGrowAccounting.name} -> ${this.deleteAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }
}