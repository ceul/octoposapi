//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { AccountsGrowAccounting } from '../../models/growAccounting/_accounts';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class AccountsDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAccount(data: AccountsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _accounts SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountsDAOGrowAccounting.name} -> ${this.insertAccount.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAccounts() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_account,
                    id_father,
                    id_account_type,
                    id_branch_office,
                    id_niif_concept,
                    id_cost_center,
                    allows_transact,
                    minimum base,
                    description,
                    sign,
                    state
                    FROM _accounts;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountsDAOGrowAccounting.name} -> ${this.getAccounts.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAccountsById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_account,
                    id_father,
                    id_account_type,
                    id_branch_office,
                    id_niif_concept,
                    id_cost_center,
                    allows_transact,
                    minimum base,
                    description,
                    sign,
                    state
                    FROM _accounts
                    WHERE id_account = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountsDAOGrowAccounting.name} -> ${this.getAccountsById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAccount(data: AccountsGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _accounts SET
                        id_account,
                        id_father,
                        id_account_type,
                        id_branch_office,
                        id_niif_concept,
                        id_cost_center,
                        allows_transact,
                        minimum_base,
                        description,
                        sign,
                        state
                        WHERE id_bank = ?;`,
                    [data.id_account,
                    data.id_father,
                    data.id_account_type,
                    data.id_branch_office,
                    data.id_niif_concept,
                    data.id_cost_center,
                    data.allows_transact,
                    data.minimum_base,
                    data.description,
                    data.sign,
                    data.state,
                    data.id_account]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountsDAOGrowAccounting.name} -> ${this.updateAccount.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAccount(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _accounts 
                    WHERE id_account = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountsDAOGrowAccounting.name} -> ${this.deleteAccount.name}: ${error}`)
            throw new Error(error)
        }
    }
}