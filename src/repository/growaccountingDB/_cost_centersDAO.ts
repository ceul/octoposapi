//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { CostCentersGrowAccounting } from '../../models/growAccounting/_cost_centers';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class CostCentersDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertCostCenter(data: CostCentersGrowAccounting, con = null) {
        try {
            let id = uuid.v4();
            data.id_cost_center = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _cost_centers SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CostCentersDAOGrowAccounting.name} -> ${this.insertCostCenter.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCostCenters() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_cost_center,
                    description,
                    state
                    FROM _cost_centers;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CostCentersDAOGrowAccounting.name} -> ${this.getCostCenters.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCostCentersById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_cost_center,
                    description,
                    state
                    FROM _cost_centers
                    WHERE id_cost_center = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CostCentersDAOGrowAccounting.name} -> ${this.getCostCentersById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCostCenter(data: CostCentersGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _cost_centers SET
                        id_cost_center = ? ,
                        description = ?,
                        state = ?
                        WHERE id_cost_center = ?;`,
                    [data.id_cost_center,
                    data.description,
                    data.state,
                    data.id_cost_center]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CostCentersDAOGrowAccounting.name} -> ${this.updateCostCenter.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteCostCenter(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _cost_centers 
                    WHERE id_cost_center = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CostCentersDAOGrowAccounting.name} -> ${this.deleteCostCenter.name}: ${error}`)
            throw new Error(error)
        }
    }
}