//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { MovementsRowGrowAccounting } from '../../models/growAccounting/_movement_row';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class MovementsRowDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertMovementsRow(data: MovementsRowGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _movement_row SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsRowDAOGrowAccounting.name} -> ${this.insertMovementsRow.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getMovementsRows() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_movement_row,
                    id_movement,
                    id_account,
                    sign,
                    state
                    FROM _movement_row;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsRowDAOGrowAccounting.name} -> ${this.getMovementsRows.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getMovementsRowsById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_movement_row,
                    id_movement,
                    id_account,
                    sign,
                    state
                    FROM _movement_row
                    WHERE id_movement_row = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsRowDAOGrowAccounting.name} -> ${this.getMovementsRowsById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async inactiveMovementsRow(data: MovementsRowGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _movement_row SET
                        state = 'i'
                        WHERE id_movement_row = ?;`,
                    [data.id_movement]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${MovementsRowDAOGrowAccounting.name} -> ${this.inactiveMovementsRow.name}: ${error}`)
            throw new Error(error)
        }
    }
}