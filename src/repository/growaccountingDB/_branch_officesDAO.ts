//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { BranchOfficesGrowAccounting } from '../../models/growAccounting/_branch-offices';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class BranchOfficesDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertBranchOffice(data: BranchOfficesGrowAccounting, con = null) {
        try {
            let id = uuid.v4();
            data.id_branch_office = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _branch_offices SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BranchOfficesDAOGrowAccounting.name} -> ${this.insertBranchOffice.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getBranchOffices() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_branch_office,
                    description,
                    state
                    FROM _branch_offices;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BranchOfficesDAOGrowAccounting.name} -> ${this.getBranchOffices.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getBranchOfficesById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_branch_office,
                    description,
                    state
                    FROM _branch_offices
                    WHERE id_branch_office = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BranchOfficesDAOGrowAccounting.name} -> ${this.getBranchOfficesById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateBranchOffice(data: BranchOfficesGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _branch_offices SET
                        id_branch_office = ? ,
                        description = ?,
                        state = ?
                        WHERE id_branch_office = ?;`,
                    [data.id_branch_office,
                    data.description,
                    data.state,
                    data.id_branch_office]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BranchOfficesDAOGrowAccounting.name} -> ${this.updateBranchOffice.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteBranchOffice(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _branch_offices 
                    WHERE id_branch_office = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BranchOfficesDAOGrowAccounting.name} -> ${this.deleteBranchOffice.name}: ${error}`)
            throw new Error(error)
        }
    }
}
