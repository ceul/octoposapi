//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { AccountTypesGrowAccounting } from '../../models/growAccounting/_account_types';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class AccountTypesDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAccountType(data: AccountTypesGrowAccounting, con = null) {
        try {
            //let id = uuid.v4();
            //data.id_account_type = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _account_types (description, state) VALUES (?, ?)', [data.description, data.state]);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountTypesDAOGrowAccounting.name} -> ${this.insertAccountType.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAccountTypes() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_account_type,
                    description,
                    state
                    FROM _account_types;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountTypesDAOGrowAccounting.name} -> ${this.getAccountTypes.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAccountTypesById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_account_type,
                    description,
                    state
                    FROM _account_types
                    WHERE id_account_type = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountTypesDAOGrowAccounting.name} -> ${this.getAccountTypesById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAccountType(data: AccountTypesGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _account_types SET
                        id_account_type = ? ,
                        description = ?,
                        state = ?
                        WHERE id_account_type = ?;`,
                    [data.id_account_type,
                    data.description,
                    data.state,
                    data.id_account_type]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountTypesDAOGrowAccounting.name} -> ${this.updateAccountType.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAccountType(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _account_types 
                    WHERE id_account_type = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AccountTypesDAOGrowAccounting.name} -> ${this.deleteAccountType.name}: ${error}`)
            throw new Error(error)
        }
    }
}