//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { BanksGrowAccounting } from '../../models/growAccounting/_banks';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class BanksDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertBank(data: BanksGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO _banks SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BanksDAOGrowAccounting.name} -> ${this.insertBank.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getBanks() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_bank,
                    description,
                    state
                    FROM _banks;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BanksDAOGrowAccounting.name} -> ${this.getBanks.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getBanksById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_bank,
                    description,
                    state
                    FROM _banks
                    WHERE id_bank = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BanksDAOGrowAccounting.name} -> ${this.getBanksById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateBank(data: BanksGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE _banks SET
                        id_bank = ? ,
                        description = ?,
                        state = ?
                        WHERE id_bank = ?;`,
                    [data.id_bank,
                    data.description,
                    data.state,
                    data.id_bank]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BanksDAOGrowAccounting.name} -> ${this.updateBank.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteBank(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM _banks 
                    WHERE id_bank = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${BanksDAOGrowAccounting.name} -> ${this.deleteBank.name}: ${error}`)
            throw new Error(error)
        }
    }
}