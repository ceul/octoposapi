
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { SharedTicketGrowPos } from 'models/growpos/sharedticket';
import { TicketGrowPos } from 'models/growpos/ticket';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class SharedTicketDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }
    
    public async getSharedTicketById(sharedTicketID: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
            id, 
            name, 
            appuser, 
            pickupid, 
            locked, 
            products
            FROM sharedtickets
            WHERE id = ?;`, [sharedTicketID]);
            if (query.length > 0) {
                query = query.map(row => (row.products = JSON.parse(row.products), row))
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.getSharedTicketById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getSharedTicketList() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
            id, 
            name, 
            appuser, 
            pickupid, 
            locked, 
            products
            FROM sharedtickets
            ORDER BY id;`);
            if (query.length > 0) {
                query = query.map(row => (row.products = JSON.parse(row.products), row))
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.getSharedTicketList.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getUserSharedTicketById(appUser: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT 
            id, 
            name, 
            appuser, 
            pickupid, 
            locked, 
            products
            FROM sharedtickets
            WHERE appuser = ? ORDER BY id;`, [appUser]);
            if (query.length > 0) {
                query = query.map(row => (row.products = JSON.parse(row.products), row))
            }
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.getUserSharedTicketById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async insertSharedTicket(id: string, ticket: TicketGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`INSERT INTO sharedtickets (id, 
                name, 
                products, 
                appuser, 
                pickupid) 
                VALUES (?,?,?,?,?);`, [id, this.getName(ticket), JSON.stringify(ticket.lines), ticket.user.id, ticket.pickupId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.insertSharedTicket.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateSharedTicket(id: string, ticket: TicketGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE sharedtickets SET 
                name = ?, 
                products = ?, 
                appuser = ?, 
                pickupid = ? 
                WHERE id = ?;`, [this.getName(ticket),
                JSON.stringify(ticket.lines),
                ticket.user.id,
                ticket.pickupId,
                    id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.updateSharedTicket.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteSharedTicket(sharedticketId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM sharedtickets 
                WHERE id = ?;`, [sharedticketId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.deleteSharedTicket.name}: ${error}`)
            throw new Error(error)
        }
    }

    public getName(ticket: TicketGrowPos, info?) {
        try {
            let name
            if (typeof ticket.date !== "object") {
                ticket.date = new Date(ticket.date)
            }

            if (ticket.user.name != null) {
                name = ticket.user.name + ' - '
            }

            if (info == null) {
                if (ticket.ticketId == 0) {
                    name += (`(${ticket.date.getHours()}:${ticket.date.getMinutes()} ${ticket.date.getMilliseconds() % 1000})`);
                } else {
                    name += (ticket.ticketId.toString());
                }
            } else {
                name += (info.toString());
            }

            if (ticket.customer != null) {
                name += ' - ' + (ticket.customer.name);
            }

            return name
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${SharedTicketDAOGrowPos.name} -> ${this.getName.name}: ${error}`)
            throw new Error(error)
        }
    }

}
