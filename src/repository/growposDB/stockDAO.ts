
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { StockDiaryGrowPos } from '../../models/growpos/stock-diary';
import { MovementReasonEnum } from '../../models/growpos/movement-reason.enum';
import async from 'async'
import * as _ from "lodash"
import * as util from "util";
import { createCipher } from 'crypto';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { StockLevelGrowPos } from 'models/growpos/stock-level';

export class StockDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    async insertStockDiarySimple(stock: StockDiaryGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            this.adjustStock(stock, con)
            let query = await con.query(`INSERT INTO stockdiary (ID, 
                DATENEW, 
                REASON, 
                LOCATION, 
                PRODUCT, 
                ATTRIBUTESETINSTANCE_ID, 
                UNITS, 
                PRICE, 
                AppUser) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);`, [stock.id,
                stock.datenew,
                stock.reason,
                stock.location,
                stock.product,
                stock.attributesetinstance_id,
                stock.units,
                stock.price,
                stock.appuser
                ]);
            if (!ban) {
                con.release()
            }
            return query

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.insertStockDiarySimple.name}: ${error}`)
            throw new Error(error)
        }
    }

    async insertStockDiary(stock: StockDiaryGrowPos[]) {
        try {
            let con = await this.connection.getConnection()
            let transaction = await con.query('START TRANSACTION').then(async () => {
                let promises = stock.map(async row => {
                    if (row.attributeinstance !== null && row.attributeinstance !== undefined && row.attributeinstance !== []) {
                        row.attributesetinstance_id = uuid.v4()
                        let description = ''
                        row.attributeinstance.forEach((item, index) => {
                            if (row.attributeinstance.length === index + 1) {
                                description = description + item.value
                            } else {
                                description = description + item.value + ','
                            }
                        })
                        let query0 = await con.query(`SELECT id FROM attributesetinstance WHERE description = ?`,
                            [description])
                        if (query0.length > 0) {
                            row.attributesetinstance_id = query0[0].id
                        } else {

                            let query = await con.query(`INSERT INTO attributesetinstance (ID, ATTRIBUTESET_ID, DESCRIPTION) VALUES (?, ?, ?)`,
                                [row.attributesetinstance_id,
                                row.productObject.attributeset_id,
                                    description
                                ])

                            this.asyncForEach(row.attributeinstance, (async (item, index) => {
                                let id = uuid.v4()
                                await con.query(`INSERT INTO attributeinstance(ID, ATTRIBUTESETINSTANCE_ID, ATTRIBUTE_ID, VALUE) VALUES (?, ?, ?, ?)`, [
                                    id,
                                    row.attributesetinstance_id,
                                    item.attribute_id,
                                    item.value
                                ])
                            }))
                        }
                    }
                    let id = uuid.v4();
                    row.id = id;
                    row.units = this.getUnitsSymmbol(row.units, row.reason)
                    // look if the product exist
                    let params = []
                    params.push(row.location)
                    params.push(row.product)
                    let sql0 = `SELECT * FROM stockcurrent WHERE location = ? 
                    AND product = ?`
                    if ((row.attributesetinstance_id !== null && row.attributesetinstance_id !== undefined) && row.attributesetinstance_id !== '') {
                        sql0 += ' AND attributesetinstance_id = ?'
                        params.push(row.attributesetinstance_id)
                    }
                    let query0 = await con.query(sql0, params)
                    params = []
                    params.push(row.units)
                    params.push(row.location)
                    params.push(row.product)
                    if (query0.length > 0) {
                        // if exist update else insert


                        let sql = `UPDATE stockcurrent SET 
                                    units = (units + ?) 
                                    WHERE location = ? 
                                    AND product = ?`
                        if ((row.attributesetinstance_id !== null && row.attributesetinstance_id !== undefined) && row.attributesetinstance_id !== '') {
                            sql += ' AND attributesetinstance_id = ?'
                        }
                        let query1 = await con.query(sql, params)
                    } else {
                        let query2 = await con.query(`INSERT INTO stockcurrent (
                                                        location,
                                                        product,
                                                        attributesetinstance_id,
                                                        units)
                                                        VALUES (?, ?, ?, ?)`,
                            [row.location,
                            row.product,
                            row.attributesetinstance_id,
                            row.units])
                    }

                    let query3 = await con.query(`INSERT INTO stockdiary (
                                                    id,
                                                    datenew,
                                                    reason,
                                                    location,
                                                    product,
                                                    attributesetinstance_id,
                                                    units,
                                                    price,
                                                    appuser,
                                                    supplier,
                                                    supplierdoc) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                        [row.id,
                        row.datenew,
                        row.reason,
                        row.location,
                        row.product,
                        row.attributesetinstance_id,
                        row.units,
                        row.price,
                        row.appuser,
                        row.supplier,
                        row.supplierdoc])
                })
                await Promise.all(promises)
                con.query('COMMIT')
                con.release();
                console.log('StockDiary insert: Transaction Complete.');
                return true
            }).catch(err => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(err)
            })
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.insertStockDiary.name}: ${error}`)
            throw new Error(error)
        }
    }

    getUnitsSymmbol(units, movementReason: number) {
        try {
            if (movementReason === MovementReasonEnum.IN_MOVEMENT || movementReason === MovementReasonEnum.IN_PURCHASE || movementReason === MovementReasonEnum.IN_REFUND) {
                return units
            } else {
                return units * (-1)
            }
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.getUnitsSymmbol.name}: ${error}`)
            throw new Error(error)
        }
    }

    async adjustStock(stock: StockDiaryGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            //Se revisa si es in producto discgregado
            /*let productBundle = getProductsBundle(stock.product);
            if (productBundle.lenght > 0) {
                productBundle.forEach(product => {
                    Object[] adjustParams = new Object[4];
                    adjustParams[0] = component.getProductBundleId();
                    adjustParams[1] = ((Object[])params)[1];
                    adjustParams[2] = ((Object[])params)[2];
                    adjustParams[3] = ((Double)((Object[])params)[3]) * component.getQuantity();
                    adjustStock(productBundle, con);
                });
            } else {*/
            // look if the product exist
            let params = []
            params.push(stock.location)
            params.push(stock.product)
            let sql0 = `SELECT * FROM stockcurrent WHERE location = ? 
                    AND product = ?`
            if ((stock.attributesetinstance_id !== null && stock.attributesetinstance_id !== undefined) && stock.attributesetinstance_id !== '') {
                sql0 += ' AND attributesetinstance_id = ?'
                params.push(stock.attributesetinstance_id)
            }
            let query0 = await con.query(sql0, params)
            params = []
            params.push(stock.units)
            params.push(stock.location)
            params.push(stock.product)
            if (query0.length > 0) {
                let sql = `UPDATE stockcurrent SET 
                                    units = (units + ?) 
                                    WHERE location = ? 
                                    AND product = ?`
                if ((stock.attributesetinstance_id !== null && stock.attributesetinstance_id !== undefined) && stock.attributesetinstance_id !== '') {
                    sql += ' AND attributesetinstance_id = ?'
                    params.push(stock.attributesetinstance_id)
                }
                let query1 = await con.query(sql, params)
            } else {
                await con.query(`INSERT INTO stockcurrent (UNITS, LOCATION, PRODUCT,
                ATTRIBUTESETINSTANCE_ID)
                VALUES (?, ?, ?, ?)`, [
                        stock.units,
                        stock.location,
                        stock.product,
                        stock.attributesetinstance_id])
            }
            if (!ban) {
                con.release()
            }
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.adjustStock.name}: ${error}`)
            throw new Error(error)
        }
    }


    async getStock() {
        try {
            let con = await this.connection.getConnection()
            let result = await con.query(`SELECT stockcurrent.location AS locationid,
            locations.name AS locationname,
            products.reference,
            products.name AS productname,
            products.category,
            categories.name AS categoryname,
            products.supplier,
            SUM(stockcurrent.units) AS units,
            products.pricebuy,
            products.pricesell,
            COALESCE(products.stockvolume,0) AS stockvolume,
            COALESCE(products.stockcost,0) AS stockcost,
            COALESCE(stocklevel.stocksecurity,0) AS stocksecurity,
            COALESCE(stocklevel.stockmaximum,0) AS stockmaximum,
            uom.name AS uom 
            FROM ((((stockcurrent INNER JOIN locations ON (stockcurrent.location = locations.id)) 
            INNER JOIN products ON (stockcurrent.product = products.id)) INNER JOIN categories ON (products.category = categories.id)) 
            INNER JOIN uom ON (uom.id = products.uom)) 
            LEFT OUTER JOIN stocklevel stocklevel ON (stockcurrent.location = stocklevel.location) 
            AND (stockcurrent.product = stocklevel.product) 
            WHERE (1=1) GROUP BY stockcurrent.location,
            locations.name,
            products.reference,
            products.name,
            products.category,
            categories.name,
            products.pricebuy,
            products.pricebuy,
            products.stockvolume,
            products.stockcost,
            stocklevel.stocksecurity,
            stocklevel.stockmaximum
            ORDER BY locationid ASC,
            categoryname ASC,
            productname ASC`)
            con.release()
            return result

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.getStock.name}: ${error}`)
            throw new Error(error)
        }
    }

    async getLowStock() {
        try {
            let con = await this.connection.getConnection()
            let result = await con.query(`SELECT stockcurrent.location AS locationid,
            locations.name AS locationname,
            products.reference,
            products.name AS productname,
            products.category,
            categories.name AS categoryname,
            products.supplier,
            SUM(stockcurrent.units) AS units,
            products.pricebuy,
            products.pricesell,
            COALESCE(products.stockvolume,0) AS stockvolume,
            COALESCE(products.stockcost,0) AS stockcost,
            COALESCE(stocklevel.stocksecurity,0) AS stocksecurity,
            COALESCE(stocklevel.stockmaximum,0) AS stockmaximum,
            suppliers.name AS suppliername,
            uom.name AS uom 
            FROM (((((stockcurrent stockcurrent INNER JOIN locations locations ON (stockcurrent.location = locations.id)) 
            INNER JOIN products products ON (stockcurrent.product = products.ID)) INNER JOIN categories categories ON (products.category = categories.ID)) 
            INNER JOIN suppliers suppliers ON (suppliers.id = products.supplier)) INNER JOIN uom uom ON (uom.id = products.uom)) 
            LEFT OUTER JOIN stocklevel stocklevel ON (stockcurrent.location = stocklevel.location) 
            AND (stockcurrent.product = stocklevel.product) WHERE (1=1)GROUP BY stockcurrent.location,
            locations.name,
            products.reference,
            products.name,
            products.category,
            categories.name,
            products.pricebuy,
            products.pricebuy,
            products.stockvolume,
            products.stockcost,
            stocklevel.stocksecurity,
            stocklevel.stockmaximum HAVING stocklevel.stocksecurity IS NOT NULL AND stocklevel.stocksecurity >= SUM(stockcurrent.units) 
            ORDER BY locationid ASC,
            categoryname ASC,
            productname ASC`)
            con.release()
            return result

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.getLowStock.name}: ${error}`)
            throw new Error(error)
        }
    }

    async getStockDiary(dateStart: Date, dateEnd: Date, supplier: string = '', reason:string = '') {
        try {
            let con = await this.connection.getConnection()
            let result = await con.query(`SELECT suppliers.NAME,
            stockdiary.DATENEW,
            products.NAME,
            products.REFERENCE,
            products.CODE,
            stockdiary.UNITS,
            stockdiary.PRICE,
            stockdiary.SUPPLIERDOC,
            stockdiary.REASON,
            sum(stockdiary.UNITS * stockdiary.PRICE) AS TOTALP,
            suppliers.id 
            FROM (stockdiary stockdiary INNER JOIN suppliers suppliers ON (stockdiary.SUPPLIER = suppliers.id)) 
            INNER JOIN products products ON (stockdiary.PRODUCT = products.ID) WHERE VISIBLE = TRUE 
            AND (stockdiary.DATENEW >= ?
            AND stockdiary.DATENEW < ?)GROUP BY stockdiary.DATENEW,
            products.NAME,
            stockdiary.REASON ORDER BY stockdiary.DATENEW DESC,
            suppliers.NAME
            `,[dateStart,dateEnd])
            con.release()
            return result

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.getStockDiary.name}: ${error}`)
            throw new Error(error)
        }
    }

    async getLocationStock() {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT stockcurrent.location AS locationid,
            locations.name AS locationname,
            products.reference AS reference,
            products.name AS productname,
            products.category,
            categories.name AS categoryname,
            products.supplier,
            SUM(stockcurrent.units) AS units,
            products.pricebuy,
            products.pricebuy * units AS buyvalue,
            products.pricesell,
            products.pricesell * units AS sellvalue,
            COALESCE(products.stockvolume,
            0) AS stockvolume,
            stockvolume * units AS volumevalue,
            COALESCE(products.stockcost,
            0) AS stockcost,
            stockcost * units AS costvalue,
            COALESCE(stocklevel.stocksecurity,
            0) AS stocksecurity,
            COALESCE(stocklevel.stockmaximum,
            0) AS stockmaximum,
            suppliers.name AS suppliername FROM ((((products products 
            CROSS JOIN categories categories ON (products.category = categories.id)) 
            CROSS JOIN stockcurrent stockcurrent ON (stockcurrent.product = products.id)) 
            CROSS JOIN locations locations ON (stockcurrent.location = locations.id)) 
            LEFT OUTER JOIN stocklevel stocklevel ON (stockcurrent.location = stocklevel.location) AND (stockcurrent.product = stocklevel.product)) 
            LEFT OUTER JOIN suppliers suppliers ON (suppliers.id = products.supplier) WHERE (1=1)GROUP BY stockcurrent.location,
            locations.name,
            products.reference,
            products.name,
            products.category,
            categories.name,
            products.pricebuy,
            products.pricesell,
            products.stockvolume,
            products.stockcost,
            stocklevel.stocksecurity,
            stocklevel.stockmaximum ORDER BY locationid ASC,
            suppliername ASC,
            categoryname ASC,
            productname ASC`)
            if (query.length > 0) {
                result = _.chain(query).groupBy("locationid").map((locationid) => {
                    let buyvalue = 0;
                    let sellvalue = 0;
                    let volumevalue = 0;
                    let costvalue = 0;
                    return {
                        name: _.get(_.find(locationid, 'locationname'), 'locationname'),
                        id: _.get(_.find(locationid, 'locationid'), 'locationid'),
                        details: _.map(locationid, (item) => {
                            buyvalue = buyvalue + item['buyvalue'];
                            sellvalue = sellvalue + item['sellvalue'];
                            volumevalue = volumevalue + item['volumevalue'];
                            costvalue = costvalue + item['costvalue'];
                            return {
                                reference: item['reference'],
                                name: item['productname'],
                                categoryname: item['categoryname'],
                                units: item['units'],
                                pricebuy: item['pricebuy'],
                                buyvalue: item['buyvalue'],
                                pricesell: item['pricesell'],
                                sellvalue: item['sellvalue'],
                                stockvolume: item['stockvolume'],
                                volumevalue: item['volumevalue'],
                                stockcost: item['stockcost'],
                                costvalue: item['costvalue'],
                                stocksecurity: item['stocksecurity'],
                                stockmaximum: item['stockmaximum'],
                                suppliername: item['suppliername'],
                            }
                        }),
                        buyvalue,
                        sellvalue,
                        volumevalue,
                        costvalue,
                    }
                }).value();
            }
            con.release()
            return result

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.getLocationStock.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async insertStockLevel(stockLevel: StockLevelGrowPos) {
        try {
            let id = uuid.v4();
            stockLevel.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO stocklevel SET ?', stockLevel);
            con.release()
            return stockLevel
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.insertStockLevel.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateStockLevel(stockLevel: StockLevelGrowPos) {
        try {
            let id = uuid.v4();
            stockLevel.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE stocklevel SET
                        stocksecurity = ? ,
                        stockmaximum = ?
                        WHERE id = ?;`,
                [stockLevel.stocksecurity,
                stockLevel.stockmaximum,
                stockLevel.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.updateStockLevel.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    async refactorStockCurrent() {
        try {
            let con = await this.connection.getConnection()
            let query0 = await con.query(`SELECT location, 
            product, 
            attributesetinstance_id, 
            SUM(units) AS units 
            FROM stockdiary GROUP BY location,product,attributesetinstance_id`)

            let query1 = await query0.map(async stock => {
                await con.query(`INSERT INTO stockcurrent (UNITS, LOCATION, PRODUCT,
                    ATTRIBUTESETINSTANCE_ID)
                    VALUES (?, ?, ?, ?)`, [
                        stock.units,
                        stock.location,
                        stock.product,
                        stock.attributesetinstance_id])
            })
            await Promise.all(query1)
            return 'TERMINE EXITOSAMENTE'
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${StockDAOGrowPos.name} -> ${this.adjustStock.name}: ${error}`)
            throw new Error(error)
        }
    }
}
