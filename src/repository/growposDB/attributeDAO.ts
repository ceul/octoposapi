
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { AttributeGrowPos } from '../../models/growpos/attribute';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { AttributeValueDAOGrowPos } from './attributeValueDAO';
import { AttributeValueGrowPos } from '../../models/growpos/attribute-value';

export class AttributeDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAttribute(attribute: AttributeGrowPos, con = null) {
        try {
            let ban = true
            attribute.id = uuid.v4()
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
                await con.query('START TRANSACTION')
            }
            let query = await con.query('INSERT INTO attribute (id, name) VALUES (?, ?) ', [attribute.id, attribute.name]);
            let attributeValueDAO = new AttributeValueDAOGrowPos()
            await this.asyncForEach(attribute.attribute_values,async (value,index) => {
                attribute.attribute_values[index] = await attributeValueDAO.insertAttributeValue(value,attribute.id,con)
            })
            if (!ban) {
                await con.query('COMMIT')
                con.release()
            }
            return attribute
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeDAOGrowPos.name} -> ${this.insertAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttribute() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM attribute;`);
            if (query.length > 0) {
                await this.asyncForEach(query, async (element, index) => {
                    let query2 = await con.query(`SELECT
                                id,
                                value
                                FROM attributevalue WHERE attribute_id = ?;`, [element.id]);
                    query[index].attribute_values = query2
                });
            }
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeDAOGrowPos.name} -> ${this.getAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeById(attributeId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM attribute
                        WHERE ID = ?;`, [attributeId]);
            if (query.length > 0) {
                let query2 = await con.query(`SELECT
                            id,
                            value
                            FROM attributevalue WHERE attribute_id = ?;`, [query[0].id]);
                query[0].attribute_values = query2
            }
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeDAOGrowPos.name} -> ${this.getAttributeById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeByAttributeSet(attributeSetId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        attribute.id,
                        attribute.name
                        FROM attribute INNER JOIN attributeuse ON attribute.id = attributeuse.attribute_id
                        WHERE attributeuse.attributeset_id = ?;`, [attributeSetId]);
            if (query.length > 0) {
                await this.asyncForEach(query, async (element, index) => {
                    let query2 = await con.query(`SELECT
                                id,
                                value
                                FROM attributevalue WHERE attribute_id = ?;`, [element.id]);
                    query[index].attribute_values = query2
                });
            }
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeDAOGrowPos.name} -> ${this.getAttributeById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAttribute(attribute: AttributeGrowPos) {
        try {
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let query = await con.query(`UPDATE attribute SET
                        name = ?
                        WHERE id = ?;`,
                [attribute.name,
                attribute.id]);
                let attributeValueDAO = new AttributeValueDAOGrowPos()
                let attributeDAO = new AttributeDAOGrowPos()
                let attributevalue: AttributeValueGrowPos
                let values = await attributeValueDAO.getAttributeByAttribute(attribute.id)
                attribute.attribute_values.forEach(async (value,index) => {
                    let arrIndex = values.findIndex(item => item.id === value.id)
                    if (arrIndex !== -1) {
                        values.splice(arrIndex,1)
                    } else {
                        attributevalue = new AttributeValueGrowPos()
                        attributevalue.value = value.value;
                        await attributeValueDAO.insertAttributeValue(attributevalue, attribute.id, con)
                    }

                })
                values.forEach(async (element) => {
                    await attributeValueDAO.deleteAttributeValueById(element.id, con)
                });
                await con.query('COMMIT')
                await con.release();
                
                return [attribute.id]
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeDAOGrowPos.name} -> ${this.updateAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttribute(attributeId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM attribute 
                        WHERE id = ?;`, [attributeId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeDAOGrowPos.name} -> ${this.deleteAttribute.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

}
