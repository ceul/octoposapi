
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { CategoryGrowPos } from '../../models/growpos/category';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import * as _ from "lodash"

export class CategoryDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async  insertCategory(category: CategoryGrowPos, con = null) {
        try {
            let id = uuid.v4();
            category.id = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO categories SET ?', category);
            if (!ban) {
                con.release()
            }
            return category
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.insertCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCategories() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name ,
                        parentid,
                        texttip,
                        catshowname,
                        catorder
                        FROM categories ORDER BY CAST(catorder AS DECIMAL);`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getCategories.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingCategories() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name ,
                        parentid,
                        texttip,
                        catshowname,
                        catorder,
                        type
                        FROM categories WHERE type = 'p' ;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getCategories.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getRootCategories() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name ,
                        parentid,
                        texttip,
                        catshowname,
                        catorder
                        FROM categories WHERE parentid IS NULL 
                        AND catshowname = TRUE ORDER BY CAST(catorder AS DECIMAL), name;`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getRootCategories.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getChildCategories(rootCategoryId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name ,
                        parentid,
                        texttip,
                        catshowname,
                        catorder
                        FROM categories WHERE PARENTID = ? ORDER BY ABS(catorder), name;`, rootCategoryId);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getChildCategories.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCategoryById(categoryId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name ,
                        parentid,
                        texttip,
                        catshowname,
                        catorder
                        FROM categories
                        WHERE id = ?;`, [categoryId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getCategoryById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCategorySalesReport(dateStart: Date, dateEnd: Date) {
        try {
            let result = []
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT categories.name AS catname,
            products.reference AS prodref,
            products.name AS prodname,
            products.pricesell,
            sum(ticketlines.units) AS totalunits,
            sum(ticketlines.units * ticketlines.price) as totalprice,
            taxes.RATE * 100 AS taxrate,
            ticketlines.price * taxes.rate AS totaltax,
            sum((ticketlines.units * ticketlines.price) + (ticketlines.price * taxes.rate)) AS totaltotal,
            closedcash.datestart AS datestart,
            closedcash.dateend AS dateend
            FROM (((((ticketlines ticketlines
            INNER JOIN taxes taxes ON (ticketlines.TAXID = taxes.ID))
            INNER JOIN tickets tickets ON (ticketlines.TICKET = tickets.ID))
            INNER JOIN receipts receipts ON (tickets.ID = receipts.ID))
            INNER JOIN closedcash closedcash ON (receipts.money = closedcash.money))
            INNER JOIN products products ON (ticketlines.product = products.ID))
            INNER JOIN categories categories ON (products.category = categories.ID)
            WHERE closedcash.dateend >= ? AND closedcash.dateend < ?
            GROUP BY products.reference`, [dateStart, dateEnd]);
            if (query.length > 0) {
                result = _.chain(query).groupBy("catname").map((catname) => {
                    let sold = 0;
                    let value = 0;
                    let taxValue = 0
                    let grossValue = 0
                    return {
                        name: _.get(_.find(catname, 'catname'), 'catname'),
                        details: _.map(catname, (item) => {
                            sold = sold + item['totalunits'];
                            value = value + item['totalprice'];
                            taxValue = taxValue + item['totaltax']
                            grossValue = grossValue + item['totaltotal']
                            return {
                                reference: item['reference'],
                                name: item['prodname'],
                                price: item['pricesell'],
                                sold: item['totalunits'],
                                value: item['totalprice'],
                                tax: item['taxrate'],
                                taxValue: item['totaltax'],
                                total: item['totaltotal'],
                            }
                        }),
                        sold,
                        value,
                        taxValue,
                        grossValue
                    }
                }).value();
            }
            con.release()
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getCategorySalesReport.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCategoryImage(productId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    image
                    FROM categories
                    WHERE id = ?;`, [productId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getCategoryImage.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCategory(category: CategoryGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE categories SET
                        name = ? ,
                        parentid = ?,
                        texttip = ?,
                        catshowname = ?,
                        catorder = ?
                        WHERE id = ?;`,
                [category.name,
                category.parentid,
                category.texttip,
                category.catshowname,
                category.catorder,
                category.id]);
            if (!ban) {
                con.release()
            }
            return query

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.updateCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async uploadPicture(id: string, img: Buffer) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE categories SET
                        image = ?
                    WHERE id = ?;`,
                [img, id])
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.uploadPicture.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteCategory(categoryId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM categories 
                    WHERE id = ?;`, [categoryId]);
            if (!ban) {
                con.release()
            }
            return query

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.deleteCategory.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCategoryBySales() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT a.NAME, 
                        sum(c.UNITS), 
                        sum(c.UNITS * (c.PRICE + (c.PRICE * d.RATE))) 
                        FROM categories as a 
                        LEFT JOIN products as b on a.id = b.CATEGORY 
                        LEFT JOIN ticketlines as c 
                        on b.id = c.PRODUCT LEFT 
                        JOIN taxes as d on c.TAXID = d.ID 
                        LEFT JOIN receipts as e on c.TICKET = e.ID
                        GROUP BY a.NAME`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CategoryDAOGrowPos.name} -> ${this.getCategoryBySales.name}: ${error}`)
            throw new Error(error)
        }
    }

}
