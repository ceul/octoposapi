
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import * as _ from "lodash"
import { LineRemovedGrowPos } from 'models/growpos/line-removed';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class LineRemovedDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertLineRemoved(lineRemoved: LineRemovedGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO lineremoved SET ?', lineRemoved);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LineRemovedDAOGrowPos.name} -> ${this.insertLineRemoved.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getLineRemoved() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        removeddate,
                        name,
                        ticketid,
                        productid,
                        productname,
                        units
                        FROM lineremoved;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LineRemovedDAOGrowPos.name} -> ${this.getLineRemoved.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getLineRemovedByUser(userId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        removeddate,
                        name,
                        ticketid,
                        productid,
                        productname,
                        units
                        FROM lineremoved
                        WHERE name = ?;`, [userId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LineRemovedDAOGrowPos.name} -> ${this.getLineRemovedByUser.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getLineRemovedByProduct(productId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        removeddate,
                        name,
                        ticketid,
                        productid,
                        productname,
                        units
                        FROM lineremoved
                        WHERE name = ?;`, [productId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LineRemovedDAOGrowPos.name} -> ${this.getLineRemovedByProduct.name}: ${error}`)
            throw new Error(error)
        }
    }
}
