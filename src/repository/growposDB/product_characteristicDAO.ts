//    Grow
//    Copyright (c) 2020 GrowPos
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { ProductCharacteristicGrowPos } from '../../models/growpos/product_characteristic';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import * as _ from "lodash"

export class ProductCharacteristicDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertProductCharacteristic(data: ProductCharacteristicGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO product_characteristic SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductCharacteristicDAOGrowPos.name} -> ${this.insertProductCharacteristic.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductCharacteristics() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_characteristic,
                    id_product,
                    val
                    FROM _attributes_accounts;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductCharacteristicDAOGrowPos.name} -> ${this.getProductCharacteristics.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductCharacteristicsById(idProduct: string, idCharacteristic: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_characteristic,
                    id_product,
                    val
                    FROM product_characteristic
                    WHERE id_product = ? and id_characteristic = ?;`, [idProduct, idCharacteristic]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductCharacteristicDAOGrowPos.name} -> ${this.getProductCharacteristicsById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductCharacteristicsByProduct(idProduct: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT  
                    pc.id_characteristic as id_characteristic, 
                    c.description as description, 
                    pc.id_product as id_product,  
                    val as val
                    FROM product_characteristic pc JOIN characteristics c 
                        ON pc.id_characteristic = c.id_characteristic
                    WHERE pc.id_product = ?;`, [idProduct]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductCharacteristicDAOGrowPos.name} -> ${this.getProductCharacteristicsByProduct.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateProductCharacteristic(data: ProductCharacteristicGrowPos, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE product_characteristic SET
                        val = ?
                        WHERE id_product = ? and id_characteristic = ?;`, [data.val, data.id_product, data.id_characteristic]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductCharacteristicDAOGrowPos.name} -> ${this.updateProductCharacteristic.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteProductCharacteristic(idCharacteristic: string, idProduct: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM product_characteristic 
                    WHERE id_product = ? and id_characteristic = ?;`, [idProduct, idCharacteristic]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ProductCharacteristicDAOGrowPos.name} -> ${this.deleteProductCharacteristic.name}: ${error}`)
            throw new Error(error)
        }
    }
}