
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { TicketGrowPos } from '../../models/growpos/ticket';
import { TicketEnum } from '../../models/growpos/ticket.enum';
import { StockDAOGrowPos } from './stockDAO';
import { StockDiaryGrowPos } from '../../models/growpos/stock-diary';
import { MovementReasonEnum } from '../../models/growpos/movement-reason.enum';
import { LineRemovedDAOGrowPos } from './lineRemovedDAO';
import { LineRemovedGrowPos } from '../../models/growpos/line-removed';
import { DrawerOpenedDAOGrowPos } from './drawerOpenedDAO';
import { DrawerOpenedGrowPos } from '../../models/growpos/drawer-opened';
import { LogDAOGrowPos } from './logDAO'
import { LogEnum } from '../../models/growpos/log.enum';
import { CustomerDAOGrowPos } from './customerDAO';
import { CreditNoteDAOGrowPos } from './credit_noteDAO';
import { CreditNoteGrowPos } from '../../models/growpos/credit_note';
import { ReceiptFilterGrowPos } from '../../models/growpos/receipt-filter';
import { FilterOperator } from '../../models/growpos/filter-operators.enum';
import { PeopleGrowPos } from '../../models/growpos/people';
import { CustomerGrowPos } from '../../models/growpos/customer';
import { TicketLineGrowPos } from '../../models/growpos/ticket-line';
import { TaxGrowPos } from '../../models/growpos/tax';
import { TaxLineGrowPos } from '../../models/growpos/tax-line';
import { PaymentGrowPos } from '../../models/growpos/payment';
import { ContractGrowPos } from 'models/growpos/contract';

export class TicketDAOGrowPos {

    private log
    private connection;
    private customerDAO: CustomerDAOGrowPos
    private creditNoteDAO: CreditNoteDAOGrowPos
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos();
        this.customerDAO = new CustomerDAOGrowPos();
        this.creditNoteDAO = new CreditNoteDAOGrowPos();
    }

    public async saveTicket(ticket: TicketGrowPos, location: string) {
        try {
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                if (ticket.ticketId == 0) {
                    switch (ticket.ticketType) {
                        case TicketEnum.RECEIPT_NORMAL:
                            ticket.ticketId = await (this.nextTicketIndex(con));
                            break;
                        case TicketEnum.RECEIPT_REFUND:
                            ticket = await (this.nextTicketRefundIndex(ticket, con));
                            break;
                        case TicketEnum.RECEIPT_PAYMENT:
                            ticket.ticketId = await (this.nextTicketPaymentIndex(con));
                            break;
                        case TicketEnum.RECEIPT_NOSALE:
                            ticket.ticketId = await (this.nextTicketPaymentIndex(con));
                            break;
                        default:
                            throw new Error();
                    }
                }
                await con.query(`INSERT INTO receipts (ID, MONEY, DATENEW, ATTRIBUTES, PERSON) VALUES (?, ?, ?, ?, ?)`, [
                    ticket.id,
                    ticket.activeCash,
                    ticket.date,
                    null,
                    ticket.user.id
                ])

                await con.query(`INSERT INTO tickets (ID, TICKETTYPE, TICKETID, PERSON, CUSTOMER, STATUS) VALUES (?, ?, ?, ?, ?, ?)`, [
                    ticket.id,
                    ticket.ticketType,
                    ticket.ticketId,
                    ticket.user.id,
                    ticket.customer !== undefined ? ticket.customer.id : null,
                    ticket.ticketstatus
                ])


                if (ticket.ticketType !== TicketEnum.RECEIPT_PAYMENT) {
                    await con.query(`UPDATE tickets SET STATUS = ? WHERE TICKETID = ?`, [ticket.ticketstatus, ticket.ticketId]);
                    let lines = await ticket.lines.map(async (line, index) => {
                        if (line.attributeinstance !== null && line.attributeinstance !== undefined && line.attributeinstance !== []) {
                            let description = ''
                            line.attributeinstance.forEach((item, index) => {
                                if (line.attributeinstance.length === index + 1) {
                                    description = description + item.value
                                } else {
                                    description = description + item.value + ','
                                }
                            })
                            let query0 = await con.query(`SELECT id FROM attributesetinstance WHERE description = ?`,
                                [description])
                            if (query0.length > 0) {
                                line.attsetinstid = query0[0].id
                            } else {
                                line.attsetinstid = uuid.v4()
                                let query = await con.query(`INSERT INTO attributesetinstance (ID, ATTRIBUTESET_ID, DESCRIPTION) VALUES (?, ?, ?)`,
                                    [line.attsetinstid,
                                    line.attributes.attributeset_id,
                                        description
                                    ])

                                this.asyncForEach(line.attributeinstance, (async (item, index) => {
                                    let id = uuid.v4()
                                    await con.query(`INSERT INTO attributeinstance(ID, ATTRIBUTESETINSTANCE_ID, ATTRIBUTE_ID, VALUE) VALUES (?, ?, ?, ?)`, [
                                        id,
                                        line.attsetinstid,
                                        item.attribute_id,
                                        item.value
                                    ])
                                }))
                            }
                        }
                        await con.query(`INSERT INTO ticketlines (TICKET, 
                        LINE, 
                        PRODUCT, 
                        ATTRIBUTESETINSTANCE_ID, 
                        UNITS, 
                        PRICE, 
                        TAXID, 
                        ATTRIBUTES) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`, [line.ticket,
                                index,
                            line.productid,
                            line.attsetinstid,
                            line.multiply,
                            line.attributes.pricesell,
                            line.tax.id,
                            JSON.stringify(line.attributes)
                            ]);

                        // Si el identificador del producto no es nulo 
                        //o el producto no es un servicio se descuenta 
                        // del inventario
                        if (line.productid != null && line.attributes.isservice != true) {
                            let stockDAO = new StockDAOGrowPos();
                            let stock = new StockDiaryGrowPos()

                            stock.id = uuid.v4();
                            stock.datenew = ticket.date;
                            stock.reason = line.multiply < 0.0 ? MovementReasonEnum.IN_REFUND : MovementReasonEnum.OUT_SALE;
                            stock.location = location;
                            stock.product = line.productid;
                            stock.attributesetinstance_id = line.attsetinstid;
                            stock.units = -line.multiply;
                            stock.price = line.attributes.pricesell;
                            stock.appuser = ticket.user.name;
                            stock.attributesetinstance_id = line.attsetinstid
                            await stockDAO.insertStockDiarySimple(stock, con)
                        }
                    })

                    await Promise.all(lines)

                    if (ticket.taxes != null) {
                        let taxes = await ticket.taxes.map(async taxLine => {
                            let id = uuid.v4();
                            let subtotal
                            let total
                            await con.query(`INSERT INTO taxlines (ID, RECEIPT, TAXID, BASE, AMOUNT)  VALUES (?, ?, ?, ?, ?)`, [
                                id,
                                ticket.id,
                                taxLine.tax.id,
                                taxLine.base,
                                taxLine.amount
                            ]);
                        })
                        await Promise.all(taxes)
                    }

                    let lineRemovedDAO = new LineRemovedDAOGrowPos();
                    let lineRemoved = new LineRemovedGrowPos()
                    lineRemoved.name = ticket.user.name
                    lineRemoved.ticketid = 'Void'
                    lineRemoved.productname = 'Ticket Deleted'
                    lineRemoved.units = 0
                    await lineRemovedDAO.insertLineRemoved(lineRemoved, con)

                } else {
                    let date = new Date()
                    await con.query(`UPDATE third_party SET CURDEBT = ?, CURDATE = ? WHERE ID = ?`, [
                        ticket.customer.curdebt,
                        `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
                        ticket.customer.id
                    ])
                }
                let payments = await ticket.payments.map(async payment => {
                    await con.query(`INSERT INTO payments (ID, 
                        RECEIPT, 
                        PAYMENT, 
                        TOTAL, 
                        TRANSID, 
                        RETURNMSG, 
                        TENDERED, 
                        CARDNAME, 
                        VOUCHER) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`, [
                            payment.id,
                            payment.receipt,
                            payment.payment,
                            payment.total,
                            payment.transid,
                            payment.returnmsg,
                            payment.tendered,
                            payment.cardname,
                            payment.voucher
                        ]);

                    // si el pago es a credito se debe aumentar la deuda
                    if ("debt" === payment.payment || "debtpaid" === payment.payment) {
                        this.customerDAO.debtUpdate(ticket.customer, con)
                    };
                })

                await Promise.all(payments)

                let drawerOpenedDAO = new DrawerOpenedDAOGrowPos();
                let drawerOpened = new DrawerOpenedGrowPos();
                drawerOpened.ticketid = ticket.ticketId.toString()
                drawerOpened.name = `${ticket.user.name} - ${drawerOpened.ticketid}`
                await drawerOpenedDAO.insertDrawerOpened(drawerOpened, con)
                // This if statement is use for vehicle contracts pay 
                if (ticket.contract !== undefined && ticket.contract !== null) {
                    let relId = uuid.v4()
                    await con.query(`INSERT INTO rel_contract_ticket ( 
                        id,
                        contract,
                        ticket ) VALUES (?, ?, ?)`, [
                            relId,
                            ticket.contract.id,
                            ticket.id
                        ]);
                }
                await con.query('COMMIT')
                await con.release();
                
                return [ticket.ticketId]
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.saveTicket.name}: ${error}`)
            throw new Error(error)
        }

    }

    public async nextTicketIndex(con) {
        try {
            await con.query('UPDATE ticketsnum SET ID = LAST_INSERT_ID(ID + 1)')
            let row = await con.query('SELECT LAST_INSERT_ID()')
            return row[0]["LAST_INSERT_ID()"]
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.nextTicketIndex.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async nextTicketRefundIndex(ticket: TicketGrowPos, con) {
        try {
            let creditNote = new CreditNoteGrowPos()
            creditNote.receipt = ticket.id
            creditNote.note = ticket.note
            creditNote.date = ticket.date
            await this.creditNoteDAO.insertCreditNote(creditNote, con)
            await con.query('UPDATE ticketsnum_refund SET ID = LAST_INSERT_ID(ID + 1)')
            let row = await con.query('SELECT LAST_INSERT_ID()')
            ticket.id = uuid.v4()
            ticket.ticketId = row[0]["LAST_INSERT_ID()"]
            ticket.lines.map(line => {
                line.ticket = ticket.id
                line.multiply = line.multiply * (-1)
            })
            ticket.taxes.map(tax => {
                tax.base = tax.base * (-1)
            })
            ticket.payments.map(payment => {
                payment.receipt = ticket.id
                payment.total = payment.total * (-1)
            })
            ticket.total = ticket.total * (-1)
            return ticket
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.nextTicketRefundIndex.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async nextTicketPaymentIndex(con) {
        try {
            await con.query('UPDATE ticketsnum_payment SET ID = LAST_INSERT_ID(ID + 1)')
            let row = await con.query('SELECT LAST_INSERT_ID()')
            return row[0]["LAST_INSERT_ID()"]
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.nextTicketPaymentIndex.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getTotalSales() {
        try {
            let con = await this.connection.getConnection()
            let date = new Date()
            let query = await con.query(`SELECT
                    SUM(payments.TOTAL) as total
                    FROM payments, receipts 
                    WHERE payments.RECEIPT = receipts.ID 
                    AND receipts.datenew >= ?`, [`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getTotalSales.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProfit() {
        try {
            let con = await this.connection.getConnection()
            let date = new Date()
            let query = await con.query(`SELECT
                    SUM((ticketlines.PRICE - products.pricebuy  ) * ticketlines.UNITS) as profit
                    FROM (ticketlines 
                        INNER JOIN products 
                        ON products.id = ticketlines.product), tickets, receipts, taxes 
                    WHERE ticketlines.TICKET = tickets.ID 
                    AND tickets.ID = receipts.ID 
                    AND ticketlines.TAXID = taxes.ID 
                    AND ticketlines.PRODUCT IS NOT NULL 
                    AND receipts.datenew >= ?
                    GROUP BY receipts.MONEY`, [`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getProfit.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getNumTicket() {
        try {
            let con = await this.connection.getConnection()
            let date = new Date()
            let query = await con.query(`SELECT
                COUNT(DISTINCT receipts.ID) as number
                FROM receipts INNER JOIN closedcash ON receipts.money = closedcash.money
                WHERE receipts.datenew >= ?`, [`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getNumTicket.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getDailySales() {
        try {
            let con = await this.connection.getConnection()
            let date = new Date()
            let query = await con.query(`SELECT
                    HOUR(receipts.datenew) as hour,
                    SUM(ticketlines.UNITS) as units,
                    SUM((ticketlines.PRICE + ticketlines.PRICE * taxes.RATE ) * ticketlines.UNITS) as total,
                    COUNT(DISTINCT receipts.ID) as receipts 
                    FROM ticketlines, tickets, receipts, taxes 
                    WHERE ticketlines.TICKET = tickets.ID
                    AND tickets.ID = receipts.ID
                    AND ticketlines.TAXID = taxes.ID
                    AND receipts.datenew >= ?
                    GROUP BY HOUR(receipts.datenew),
                    DAY(receipts.datenew)`, [`${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getDailySales.name}: ${error}`)
            throw new Error(error)
        }
    }

    async getTotalSalesByCloseCash(closeCash: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT COUNT(DISTINCT receipts.ID) as sales, 
            SUM(ticketlines.UNITS * ticketlines.PRICE) as total
            FROM receipts, ticketlines 
            WHERE receipts.ID = ticketlines.TICKET 
            AND receipts.MONEY = ?`, [closeCash]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getTotalSalesByCloseCash.name}: ${error}`)
            throw new Error(error)
        }
    }

    async getReceiptById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let ticketQuery = await con.query(`SELECT T.id as id,
            T.tickettype as tickettype,
            T.ticketid as ticketid,
            R.datenew as date,
            R.money as activeCash,
            P.id as userid,
            P.name as username,
            T.customer as customer,
            T.status as ticketstatus
            FROM receipts R JOIN tickets T ON R.id = T.id 
            LEFT OUTER JOIN people P ON T.PERSON = P.id 
            WHERE T.id = ? 
            ORDER BY R.datenew DESC`, [id]);

            let ticketLinesQuery = await con.query(`SELECT L.ticket,
            L.line,
            L.product,
            L.attributesetinstance_id,
            L.units,
            L.price,
            T.id,
            T.name,
            T.category,
            T.custcategory,
            T.parentid,
            T.rate,
            T.ratecascade,
            T.rateorder,
            L.attributes FROM ticketlines L, taxes T 
            WHERE L.taxid = T.id AND L.ticket = ? ORDER BY L.line `, [id]);

            let paymentsQuery = await con.query(`SELECT payment,
            total,
            transid,
            tendered,
            cardname FROM payments 
            WHERE receipt = ?`, [id]);
            let ticket
            if (ticketQuery.length > 0) {
                let isRefund = await con.query(`SELECT id
                FROM tickets WHERE status = ?`, [ticketQuery[0].ticketid]);
                ticket = new TicketGrowPos()
                ticket.id = ticketQuery[0].id
                ticket.ticketType = ticketQuery[0].tickettype
                ticket.ticketId = ticketQuery[0].ticketid
                ticket.date = ticketQuery[0].date
                ticket.activeCash = ticketQuery[0].activeCash
                ticket.user = new PeopleGrowPos()
                ticket.user.name = ticketQuery[0].username
                ticket.user.id = ticketQuery[0].userid
                if (isRefund.length > 0) {
                    ticket.isRefund = true
                }
                if (ticketQuery[0].customer !== null) {
                    ticket.customer = new CustomerGrowPos()
                    ticket.customer.id = ticketQuery[0].customer
                    let customer = await con.query(`SELECT id,
                    name,
                    taxid,
                    address,
                    email,
                    phone,
                    curdebt
                    FROM third_party WHERE id = ?`, [ticketQuery[0].customer]);
                    ticket.customer.name = customer[0].name
                    ticket.customer.taxid = customer[0].taxid
                    ticket.customer.address = customer[0].address
                    ticket.customer.email = customer[0].email
                    ticket.customer.phone = customer[0].phone
                    ticket.customer.curdebt = customer[0].curdebt
                }
                ticket.ticketstatus = ticketQuery[0].ticketstatus
                ticket.lines = []
                ticket.taxes = []
                ticket.payments = []
                ticket.totalTaxes = 0
                ticket.total = 0
                if (ticketLinesQuery.length > 0) {
                    ticketLinesQuery.forEach(line => {
                        let ticketLine = new TicketLineGrowPos()

                        ticketLine.line = line.line
                        ticketLine.productid = line.product
                        ticketLine.attsetinstid = line.attributesetinstance_id
                        ticketLine.multiply = line.units
                        ticketLine.price = line.price
                        ticketLine.attributes = JSON.parse(line.attributes)
                        ticket.total += line.price * line.units
                        let tax = new TaxGrowPos()
                        tax.id = line.id
                        tax.name = line.name
                        tax.taxcategoryid = line.category
                        tax.taxcustcategoryid = line.custcategory
                        tax.parentid = line.parentid
                        tax.rate = line.rate
                        tax.cascade = line.ratecascade
                        tax.order = line.rateorder

                        ticketLine.tax = tax
                        ticket.lines.push(ticketLine)

                        let index = ticket.taxes.findIndex(taxline => taxline.tax.id === tax.id)
                        if (index > -1) {
                            ticket.taxes[index].base += ticketLine.price * ticketLine.multiply
                            ticket.taxes[index].amount += (ticketLine.price * ticketLine.multiply) * tax.rate
                        } else {
                            let taxLine = new TaxLineGrowPos()
                            taxLine.tax = ticketLine.tax
                            taxLine.base = ticketLine.price * ticketLine.multiply
                            taxLine.amount = (ticketLine.price * ticketLine.multiply) * tax.rate
                            ticket.taxes.push(taxLine)
                        }
                        ticket.totalTaxes += (ticketLine.price * ticketLine.multiply) * tax.rate
                    });
                }
                if (paymentsQuery.length > 0) {
                    paymentsQuery.forEach(paymentLine => {
                        let payment = new PaymentGrowPos()
                        payment.payment = paymentLine.payment
                        payment.total = paymentLine.total
                        payment.transid = paymentLine.transid
                        payment.tendered = paymentLine.tendered
                        payment.cardname = paymentLine.cardname
                        ticket.payments.push(payment)
                    });
                }
            } else {
                ticket = []
            }
            con.release();
            return [ticket]
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getReceiptById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getReceiptFiltered(filter: ReceiptFilterGrowPos) {
        try {
            let params = []
            let sql = `SELECT T.ID as id,
            T.TICKETID as ticketid,
            T.TICKETTYPE as tickettype,
            R.DATENEW as date,
            P.NAME as employee,
            C.NAME as customer,
            SUM(PM.TOTAL) as total,
            T.STATUS as status
            FROM receipts R JOIN tickets T ON R.ID = T.ID LEFT OUTER JOIN payments PM ON R.ID = PM.RECEIPT 
            LEFT OUTER JOIN third_party C ON C.ID = T.CUSTOMER LEFT OUTER JOIN people P ON T.PERSON = P.ID WHERE (1=1`
            if (filter.dateStart !== undefined && filter.dateEnd !== undefined) {
                sql += ' and R.datenew >= ? AND R.datenew < ?'
                params.push(filter.dateStart)
                params.push(filter.dateEnd)
            }

            if (filter.ticket !== undefined) {
                if (filter.ticket !== '') {
                    sql += ' and T.ticketid = ?'
                    params.push(filter.ticket)
                }
            }
            if (filter.ticketType !== undefined) {
                if (filter.ticketType !== '') {
                    sql += ' and T.tickettype = ?'
                    params.push(filter.ticketType)
                }
            }
            if (filter.customer !== undefined) {
                if (filter.customer !== '') {
                    sql += ' and C.id = ?'
                    params.push(filter.customer)
                }
            }
            if (filter.user !== undefined) {
                if (filter.user !== '') {
                    sql += ' and P.id = ?'
                    params.push(filter.user)
                }
            }
            if (filter.total !== undefined) {
                if (filter.total.filter !== FilterOperator.None && filter.total.value !== '') {
                    let op = this.getOperator(filter.total.filter)
                    sql += ` and PM.total ${op} ?`
                    params.push(filter.total.value)
                }
            }
            sql += `) GROUP BY T.ID,
            T.TICKETID,
            T.TICKETTYPE,
            R.DATENEW,
            P.NAME,
            C.NAME ORDER BY R.DATENEW DESC,
            T.TICKETID`
            let con = await this.connection.getConnection()
            let query = await con.query(sql, params)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getReceiptFiltered.name}: ${error}`)
            throw new Error(error)
        }
    }

    public getOperator(operator: number) {
        try {
            if (operator === FilterOperator.None) {
                return
            } else if (operator === FilterOperator.Equals) {
                return '='
            } else if (operator === FilterOperator.Distinct) {
                return '<>'
            } else if (operator === FilterOperator.Greater) {
                return '>'
            } else if (operator === FilterOperator.Less) {
                return '<'
            } else if (operator === FilterOperator.GreaterOrEqual) {
                return '>='
            } else if (operator === FilterOperator.LessOrEqual) {
                return '<='
            }
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${TicketDAOGrowPos.name} -> ${this.getOperator.name}: ${error}`)
            throw new Error(error)
        }
    }
    public async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }
}