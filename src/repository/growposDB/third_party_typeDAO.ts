//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { ThirdPartyTypeGrowAccounting } from '../../models/growpos/third_party_type';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import * as _ from "lodash"

export class ThirdPartyTypesDAOGrowAccounting {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertThirdPartyType(data: ThirdPartyTypeGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO third_party_type SET ?', data);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ThirdPartyTypesDAOGrowAccounting.name} -> ${this.insertThirdPartyType.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getThirdPartyTypes() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_third_party_type,
                    description,
                    state
                    FROM third_party_type;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ThirdPartyTypesDAOGrowAccounting.name} -> ${this.getThirdPartyTypes.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getThirdPartyTypeById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_third_party_type,
                    description,
                    state
                    FROM third_party_type
                    WHERE id_third_party_type = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ThirdPartyTypesDAOGrowAccounting.name} -> ${this.getThirdPartyTypeById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateThirdPartyType(data: ThirdPartyTypeGrowAccounting, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE third_party_type SET
                        id_third_party_type = ? ,
                        description = ?,
                        state = ?
                        WHERE id_third_party_type = ?;`,
                    [data.id_third_party_type,
                    data.description,
                    data.state,
                    data.id_third_party_type]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ThirdPartyTypesDAOGrowAccounting.name} -> ${this.updateThirdPartyType.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteThirdPartyType(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM third_party_type 
                    WHERE id_third_party_type = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ThirdPartyTypesDAOGrowAccounting.name} -> ${this.deleteThirdPartyType.name}: ${error}`)
            throw new Error(error)
        }
    }
}