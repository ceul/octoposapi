
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { ParkingFeeGrowPos } from 'models/growpos/parkingFee';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import { CategoryDAOGrowPos } from './categoryDAO';
import { ProductDAOGrowPos } from './productDAO';
import * as _ from "lodash"

export class ParkingFeeDAOGrowPos {

    private log
    private connection;
    private categoryDAO: CategoryDAOGrowPos
    private productDAO: ProductDAOGrowPos
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
        this.categoryDAO = new CategoryDAOGrowPos()
        this.productDAO = new ProductDAOGrowPos()
    }

    public async insertParkingFee(parkingFees: ParkingFeeGrowPos[]) {
        try {
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let cat = await this.categoryDAO.insertCategory(parkingFees[0].type, con)
                let fees = await parkingFees.map(async (parkingFee, index) => {
                    parkingFee.rate.category = cat.id
                    let product = await this.productDAO.insertProduct(parkingFee.rate, con)
                    let id = uuid.v4();
                    parkingFee.id = id;
                    let query = await con.query(`INSERT INTO parkingfee (id,
                        starttime,
                        endtime,
                        type,
                        rate,
                        graceperiod,
                        time,
                        isMonthly) VALUES (?,?,?,?,?,?,?,?);`, [parkingFee.id,
                        parkingFee.starttime,
                        parkingFee.endtime,
                        cat.id,
                        product.id,
                        parkingFee.graceperiod,
                        parkingFee.time,
                        parkingFee.isMonthly]);
                    parkingFees[index] = parkingFee
                })
                await Promise.all(fees)
                await con.query('COMMIT')
                con.release()
                return parkingFees
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.insertParkingFee.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingFee(con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`SELECT
                        id,
                        starttime,
                        endtime,
                        type,
                        rate,
                        graceperiod,
                        time,
                        isMonthly
                        FROM parkingfee;`);

            let parkingFees = await query.map(async (parkingFee, index) => {
                let type = await this.categoryDAO.getCategoryById(parkingFee.type)
                let rate = await this.productDAO.getProductById(parkingFee.rate)
                query[index].type = type[0]
                query[index].rate = rate[0]
            })
            await Promise.all(parkingFees)
            if (!ban) {
                con.release()
            }
            return parkingFees
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.getParkingFee.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingFeeGroupByType(con = null) {
        try {
            let ban = true
            let result = []
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`SELECT
                        parkingfee.id as id,
                        parkingfee.starttime as starttime,
                        parkingfee.endtime as endtime,
                        parkingfee.type as type,
                        parkingfee.rate as rate,
                        parkingfee.graceperiod as graceperiod,
                        parkingfee.time as time,
                        parkingfee.isMonthly as isMonthly,
                        products.id as productid,
                        products.reference as reference,
                        products.code as code,
                        products.codetype as codetype,
                        products.name as productname,
                        products.pricebuy as pricebuy,
                        products.pricesell as pricesell,
                        products.category as category,
                        products.taxcat as taxcat,
                        products.attributeset_id as attributeset_id,
                        products.stockcost as stockcost,
                        products.stockvolume as stockvolume,
                        products.iscom as iscom,
                        products.isscale as isscale,
                        products.isconstant as isconstant,
                        products.printkb as printkb,
                        products.sendstatus as sendstatus,
                        products.isservice as isservice,
                        products.attributes as attributes,
                        products.display as display,
                        products.isvprice as isvprice,
                        products.isverpatrib as isverpatrib,
                        products.texttip as texttip,
                        products.warranty as warranty,
                        products.stockunits as stockunits,
                        products.printto as printto,
                        products.supplier as supplier,
                        products.uom as uom,
                        products.flag as flag,
                        products.description as description,
                        products.short_description as short_description,
                        products.weigth as weigth,
                        products.length as length,
                        products.height as height,
                        products.width as width,
                        categories.id as categoryid,
                        categories.name as categoryname ,
                        categories.parentid as parentid,
                        categories.texttip as texttip,
                        categories.catshowname as catshowname,
                        categories.catorder as catorder
                        FROM ((parkingfee INNER JOIN categories ON (parkingfee.type = categories.id)) 
                        INNER JOIN products ON parkingfee.rate = products.id) ORDER BY type ASC;`);

            if (query.length > 0) {
                result = _.chain(query).groupBy("type").map((parkingFee) => {
                    return {
                        id: _.get(_.find(parkingFee, 'categoryid'), 'categoryid'),
                        name: _.get(_.find(parkingFee, 'categoryname'), 'categoryname') ,
                        parentid: _.get(_.find(parkingFee, 'parentid'), 'parentid'),
                        texttip: _.get(_.find(parkingFee, 'texttip'), 'texttip'),
                        catshowname: _.get(_.find(parkingFee, 'catshowname'), 'catshowname'),
                        catorder: _.get(_.find(parkingFee, 'catorder'), 'catorder'),

                        parkingFees: _.map(parkingFee, (item) => {
                            return {
                                id: item['id'],
                                starttime: item['starttime'],
                                endtime: item['endtime'],
                                type: item['type'],
                                rate: {
                                    id: item['productid'],
                                    reference: item['reference'],
                                    code: item['code'],
                                    codetype: item['codetype'],
                                    name: item['productname'],
                                    pricebuy: item['pricebuy'],
                                    pricesell: item['pricesell'],
                                    category: item['category'],
                                    taxcat: item['taxcat'],
                                    attributeset_id: item['attributeset_id'],
                                    stockcost: item['stockcost'],
                                    stockvolume: item['stockvolume'],
                                    iscom: item['iscom'],
                                    isscale: item['isscale'],
                                    isconstant: item['isconstant'],
                                    printkb: item['printkb'],
                                    sendstatus: item['sendstatus'],
                                    isservice: item['isservice'],
                                    attributes: item['attributes'],
                                    display: item['display'],
                                    isvprice: item['isvprice'],
                                    isverpatrib: item['isverpatrib'],
                                    texttip: item['texttip'],
                                    warranty: item['warranty'],
                                    stockunits: item['stockunits'],
                                    printto: item['printto'],
                                    supplier: item['supplier'],
                                    uom: item['uom'],
                                    flag: item['flag'],
                                    description: item['description'],
                                    short_description: item['short_description'],
                                    weigth: item['weigth'],
                                    length: item['length'],
                                    height: item['height'],
                                    width: item['width'],
                                },
                                graceperiod: item['graceperiod'],
                                time: item['time'],
                                isMonthly: item['isMonthly'],
                            }
                        })
                    }
                }).value();
            }
            if (!ban) {
                con.release()
            }
            return result
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.getParkingFee.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingFeeById(parkingFeeId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`SELECT
                        id,
                        starttime,
                        endtime,
                        type,
                        rate,
                        graceperiod,
                        time,
                        isMonthly,
                        products.uom as unittime
                        FROM parkingfee INNER JOIN products ON parkingfee.rate = products.id
                        WHERE ID = ?;`, [parkingFeeId]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.getParkingFeeById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getParkingFeeByType(parkingFeeType: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`SELECT
                        id,
                        starttime,
                        endtime,
                        type,
                        rate,
                        graceperiod,
                        time,
                        isMonthly
                        FROM parkingfee
                        WHERE type = ?;`, [parkingFeeType]);
            let type = await this.categoryDAO.getCategoryById(query[0].type)
            let parkingFees = await query.map(async (parkingFee, index) => {
                let rate = await this.productDAO.getProductById(parkingFee.rate)
                query[index].type = type[0]
                query[index].rate = rate[0]
            })
            await Promise.all(parkingFees)
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.getParkingFeeByType.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateParkingFee(parkingFees: ParkingFeeGrowPos[]) {
        try {
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let cat = await this.categoryDAO.updateCategory(parkingFees[0].type, con)
                let values = await this.getParkingFeeByType(parkingFees[0].type.id, con)
                let fees = await parkingFees.map(async (parkingFee, index) => {

                    let arrIndex = values.findIndex(item => item.id === parkingFee.id)
                    if (arrIndex !== -1) {
                        let query = await con.query(`UPDATE parkingfee SET
                            starttime = ?,
                            endtime = ?,
                            graceperiod = ?,
                            time = ?,
                            isMonthly = ? WHERE id = ?;`, [
                                parkingFee.starttime,
                                parkingFee.endtime,
                                parkingFee.graceperiod,
                                parkingFee.time,
                                parkingFee.isMonthly,
                                parkingFee.id]);
                        await this.productDAO.updateProduct(parkingFee.rate, con)
                        let index = values.findIndex(item => item.id === parkingFee.id)
                        values.splice(index, 1)

                    } else {

                        parkingFee.rate.category = parkingFee.type.id
                        let product = await this.productDAO.insertProduct(parkingFee.rate, con)
                        let id = uuid.v4();
                        parkingFee.id = id;
                        parkingFees[index].rate = product
                        parkingFees[index].id = id
                        let query = await con.query(`INSERT INTO parkingfee (id,
                        starttime,
                        endtime,
                        type,
                        rate,
                        graceperiod,
                        time,
                        isMonthly) VALUES (?,?,?,?,?,?,?,?);`, [parkingFee.id,
                            parkingFee.starttime,
                            parkingFee.endtime,
                            parkingFee.type.id,
                            product.id,
                            parkingFee.graceperiod,
                            parkingFee.time,
                            parkingFee.isMonthly]);
                    }
                })
                await Promise.all(fees)
                let deleteValues = values.map(async (element) => {
                    let query = await con.query(`DELETE FROM parkingfee
                    WHERE rate = ?;`, [element.rate.id]);
                    await this.productDAO.deleteProduct(element.rate.id, con)
                });
                await Promise.all(deleteValues)
                await con.query('COMMIT')
                await con.release();
                
                return parkingFees
            }).catch(error => {
                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });

        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.updateParkingFee.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteParkingFee(parkingFeeType: string) {
        try {
            let con = await this.connection.getConnection()
            return await con.query('START TRANSACTION').then(async transaction => {
                let parkingFees = await con.query(`SELECT
                id,
                starttime,
                endtime,
                type,
                rate,
                graceperiod,
                time,
                isMonthly
                FROM parkingfee
                WHERE type = ?;`, [parkingFeeType]);

                let query = await con.query(`DELETE FROM parkingfee
                WHERE type = ?;`, [parkingFeeType]);

                let query1 = await parkingFees.map(async parkingFee => {
                    await this.productDAO.deleteProductByCategory(parkingFee.type, con)
                })
                await Promise.all(query1)
                await this.categoryDAO.deleteCategory(parkingFeeType, con)
                await con.query('COMMIT')
                con.release()
                return query
            }).catch(error => {

                con.rollback(() => {
                    con.release();
                });
                throw new Error(error)
            });
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${ParkingFeeDAOGrowPos.name} -> ${this.deleteParkingFee.name}: ${error}`)
            throw new Error(error)
        }
    }
}
