//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of Grow
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { CharacteristicGroweCommerce } from '../../models/growpos/characteristic';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from '../growposDB/logDAO';
import * as _ from "lodash"

export class CharacteristicsDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertCharacteristic(data: CharacteristicGroweCommerce, con = null) {
        try {
            //let id = uuid.v4();
            //data.id_account_type = id;
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO characteristics (description, state) VALUES (?, ?)', [data.description, data.state]);
            if (!ban) {
                con.release()
            }
            return data
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CharacteristicsDAOGrowPos.name} -> ${this.insertCharacteristic.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCharacteristics() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_characteristic,
                    description,
                    state
                    FROM characteristics;`)
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CharacteristicsDAOGrowPos.name} -> ${this.getCharacteristics.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getCharacteristicsById(id: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                    id_characteristic,
                    description,
                    state
                    FROM characteristics
                    WHERE id_characteristic = ?;`, [id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CharacteristicsDAOGrowPos.name} -> ${this.getCharacteristicsById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateCharacteristic(data: CharacteristicGroweCommerce, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`UPDATE characteristics SET
                        id_characteristic = ? ,
                        description = ?,
                        state = ?
                        WHERE id_characteristic = ?;`,
                    [data.id_characteristic,
                    data.description,
                    data.state,
                    data.id_characteristic]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CharacteristicsDAOGrowPos.name} -> ${this.updateCharacteristic.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteCharacteristic(id: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM characteristics 
                    WHERE id_characteristic = ?;`, [id]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${CharacteristicsDAOGrowPos.name} -> ${this.deleteCharacteristic.name}: ${error}`)
            throw new Error(error)
        }
    }
}