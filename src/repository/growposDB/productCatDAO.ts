
import { DataBaseService } from '../../dataBaseService';
import { ProductCatGrowPos } from 'models/growpos/product-cat';
import { LogEnum } from 'models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class ProductCatDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }
    
    public async insertProductCat(productCat: ProductCatGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO products_cat SET ?', [productCat]);
            con.release();
            return productCat
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${ProductCatDAOGrowPos.name} -> ${this.insertProductCat.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductCat() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        product,
                        catorder
		                FROM products_cat;`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${ProductCatDAOGrowPos.name} -> ${this.getProductCat.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getProductCatById(productCatId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        product,
                        catorder
                        FROM products_cat
                        WHERE product = ?;`, [productCatId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${ProductCatDAOGrowPos.name} -> ${this.getProductCatById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateProductCat(productCat: ProductCatGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE products_cat SET
                        catorder = ?
                        WHERE product = ?;`,
                [productCat.catorder,
                productCat.product]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${ProductCatDAOGrowPos.name} -> ${this.updateProductCat.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteProductCat(productCatId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM products_cat 
                        WHERE product = ?;`, [productCatId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR,`${ProductCatDAOGrowPos.name} -> ${this.deleteProductCat.name}: ${error}`)
            throw new Error(error)
        }
    }

}
