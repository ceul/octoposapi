
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';
import AttributeUseGrowPos from 'models/growpos/attribute-use';

export class AttributeUseDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertAttributeUse(attributeUse: AttributeUseGrowPos, con = null) {
        try {
            let ban = true
            attributeUse.id = uuid.v4();
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query('INSERT INTO attributeuse (id, attributeset_id, attribute_id,lineno) VALUES (?,?,?,?)', [attributeUse.id,
            attributeUse.attributeset_id,
            attributeUse.attribute.id,
            attributeUse.line]);
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeUseDAOGrowPos.name} -> ${this.insertAttributeUse.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeUse() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM attributeuse;`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeUseDAOGrowPos.name} -> ${this.getAttributeUse.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getAttributeUseById(attributeUseId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name
                        FROM attributeuse
                        WHERE ID = ?;`, { attributeUseId });
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeUseDAOGrowPos.name} -> ${this.getAttributeUseById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateAttributeUse(attributeUse: AttributeUseGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE attributeuse SET
                        lineno = ?
                        WHERE id = ?;`,
                [attributeUse.line,
                attributeUse.id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeUseDAOGrowPos.name} -> ${this.updateAttributeUse.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttributeUseById(attributeUseId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM attributeuse 
                        WHERE id = ?;`, { attributeUseId });
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeUseDAOGrowPos.name} -> ${this.deleteAttributeUseById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteAttributeUseByAttributeAndAttributeSet(attributeId: string, attributeSetId: string, con = null) {
        try {
            let ban = true
            if (con === null) {
                ban = false
                con = await this.connection.getConnection()
            }
            let query = await con.query(`DELETE FROM attributeuse 
                        WHERE attribute_id = ? AND attributeset_id = ?;`, [attributeId, attributeSetId] );
            if (!ban) {
                con.release()
            }
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${AttributeUseDAOGrowPos.name} -> ${this.deleteAttributeUseById.name}: ${error}`)
            throw new Error(error)
        }
    }

}
