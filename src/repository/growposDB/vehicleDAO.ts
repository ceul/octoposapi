
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { VehicleGrowPos } from '../../models/growpos/vehicle';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class VehicleDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertVehicle(vehicle: VehicleGrowPos) {
        try {
            let id = uuid.v4();
            vehicle.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO vehicles SET ?', vehicle);
            con.release()
            return vehicle
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${VehicleDAOGrowPos.name} -> ${this.insertVehicle.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getVehicle() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        plate,
                        model,
                        color,
                        description, 
                        type
                        FROM vehicles;`);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${VehicleDAOGrowPos.name} -> ${this.getVehicle.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getVehicleById(vehicleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        plate,
                        model,
                        color,
                        description, 
                        type
                        FROM vehicles
                        WHERE id = ?;`, [vehicleId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${VehicleDAOGrowPos.name} -> ${this.getVehicleById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getVehicleByPlate(plate: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        plate,
                        model,
                        color,
                        description, 
                        type
                        FROM vehicles
                        WHERE plate = ?;`, [plate]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${VehicleDAOGrowPos.name} -> ${this.getVehicleByPlate.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateVehicle(vehicle: VehicleGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE vehicles SET
                        id = ?,
                        plate = ?,
                        model = ?,
                        color = ?,
                        description = ?, 
                        type = ?
                        WHERE id = ?;`,
                [vehicle.id,
                vehicle.plate,
                vehicle.model,
                vehicle.color,
                vehicle.description,
                vehicle.type,
                vehicle.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${VehicleDAOGrowPos.name} -> ${this.updateVehicle.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteVehicle(vehicleId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM vehicles 
                        WHERE id = ?;`, [vehicleId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${VehicleDAOGrowPos.name} -> ${this.deleteVehicle.name}: ${error}`)
            throw new Error(error)
        }
    }
}
