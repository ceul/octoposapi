
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { LocationGrowPos } from 'models/growpos/location';
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class LocationDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertLocation(location: LocationGrowPos) {
        try {
            let id = uuid.v4();
            location.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO locations SET ?', [location]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LocationDAOGrowPos.name} -> ${this.insertLocation.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getLocation() {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        address
                        FROM locations ORDER BY name;`);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LocationDAOGrowPos.name} -> ${this.getLocation.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getLocationById(locationId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        address
                        FROM locations
                        WHERE ID = ?;`, [locationId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LocationDAOGrowPos.name} -> ${this.getLocationById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateLocation(location: LocationGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE locations SET
                        name = ?
                        address = ?
                        WHERE id = ?;`,
                [location.name,
                location.address,
                location.id]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LocationDAOGrowPos.name} -> ${this.updateLocation.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteLocation(locationId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM locations 
                        WHERE id = ?;`, [locationId]);
            con.release();
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${LocationDAOGrowPos.name} -> ${this.deleteLocation.name}: ${error}`)
            throw new Error(error)
        }
    }

}
