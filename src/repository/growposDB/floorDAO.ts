
import { DataBaseService } from '../../dataBaseService';
import * as uuid from "uuid";
import { FloorGrowPos } from 'models/growpos/floor';
import * as _ from "lodash"
import { LogEnum } from '../../models/growpos/log.enum';
import { LogDAOGrowPos } from './logDAO';

export class FloorDAOGrowPos {

    private log
    private connection;
    constructor() {
        this.connection = DataBaseService.getInstance();
        this.log = new LogDAOGrowPos()
    }

    public async insertFloor(floor: FloorGrowPos) {
        try {
            let id = uuid.v4();
            floor.id = id;
            let con = await this.connection.getConnection()
            let query = await con.query('INSERT INTO floors SET ?', floor);
            con.release()
            return floor
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${FloorDAOGrowPos.name} -> ${this.insertFloor.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getFloor(type: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        type
                        FROM floors WHERE type = ?;`, [type]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${FloorDAOGrowPos.name} -> ${this.getFloor.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getFloorById(FloorId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT
                        id,
                        name,
                        type
                        FROM floors
                        WHERE id = ?;`, [FloorId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${FloorDAOGrowPos.name} -> ${this.getFloorById.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async getFloorsWithPlace(type: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`SELECT id, 
                        name,
                        type
                        FROM floors WHERE type=? ORDER BY name;`, [type]);
            let floors = await query.map(async (floor, index) => {
                let places = await con.query(`SELECT id, 
                            name, 
                            seats, 
                            x, 
                            y, 
                            floor, 
                            customer, 
                            waiter, 
                            ticketid, 
                            tablemoved, 
                            width, 
                            height, 
                            guests, 
                            occupied FROM places 
                            WHERE floor = ? ORDER BY x`, [floor.id]);
                if (places.length > 0) {
                    query[index]['places'] = places
                } else {
                    query[index]['places'] = []
                }

            })
            await Promise.all(floors)

            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${FloorDAOGrowPos.name} -> ${this.getFloorsWithPlace.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async updateFloor(floor: FloorGrowPos) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`UPDATE floors SET
                        name = ?,
                        type = ?
                        WHERE id = ?;`,
                [floor.name,
                 floor.type,
                floor.id]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${FloorDAOGrowPos.name} -> ${this.updateFloor.name}: ${error}`)
            throw new Error(error)
        }
    }

    public async deleteFloor(FloorId: string) {
        try {
            let con = await this.connection.getConnection()
            let query = await con.query(`DELETE FROM floors 
                        WHERE id = ?;`, [FloorId]);
            con.release()
            return query
        } catch (error) {
            this.log.insertLog(LogEnum.ERROR, `${FloorDAOGrowPos.name} -> ${this.deleteFloor.name}: ${error}`)
            throw new Error(error)
        }
    }

}
