
import { Request, Response } from 'express';
import { TicketDAOGrowPos } from '../repository/growposDB/ticketDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let ticket = new TicketDAOGrowPos();
export class TicketController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async saveTicket(req: Request, res: Response, next) {
		try {
			res.send(await ticket.saveTicket(req.body.ticket, req.body.location));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while saving ticket :' + error + `: ${TicketController.name} -> saveTicket`);
		}
	}

	public async getTotalSales(req: Request, res: Response, next) {
		try {
			res.send(await ticket.getTotalSales());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while get total sales :' + error + `: ${TicketController.name} -> getTotalSales`);
		}
	}

	public async getProfit(req: Request, res: Response, next) {
		try {
			res.send(await ticket.getProfit());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while get profit :' + error + `: ${TicketController.name} -> getProfit`);
		}
	}

	public async getNumTicket(req: Request, res: Response, next) {
		try {
			res.send(await ticket.getNumTicket());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while get number ticket :' + error + `: ${TicketController.name} -> getNumTicket`);
		}
	}

	public async getDailySales(req: Request, res: Response, next) {
		try {
			res.send(await ticket.getDailySales());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while get daily sales :' + error + `: ${TicketController.name} -> getNumTicket`);
		}
	}

	public async getTotalSalesByCloseCash(req: Request, res: Response, next){
		try {
			res.send(await ticket.getTotalSalesByCloseCash(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while get total sale by cash :' + error + `: ${TicketController.name} -> getTotalSalesByCloseCash`);
		}
	}

	public async getReceiptFiltered(req: Request, res: Response, next){
		try {
			res.send(await ticket.getReceiptFiltered(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting receipt filtered :' + error + `: ${TicketController.name} -> getReceiptFiltered`);
		}
	}

	public async getReceiptById(req: Request, res: Response, next){
		try {
			res.send(await ticket.getReceiptById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting receipt by id :' + error + `: ${TicketController.name} -> getReceiptById`);
		}
	}
	
}

