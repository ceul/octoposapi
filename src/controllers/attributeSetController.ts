
import { Request, Response } from 'express';
import { AttributeSetDAOGrowPos } from '../repository/growposDB/attributeSetDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let attributeSet = new AttributeSetDAOGrowPos();
export class AttributeSetController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertAttributeSet(req: Request, res: Response, next) {
		try {
			res.send(await attributeSet.insertAttributeSet(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting attributeSet :' + error + `: ${AttributeSetController.name} -> insertAttributeSet`);
		}
	}

	public async getAttributeSet(req: Request, res: Response, next) {
		try {
			res.send(await attributeSet.getAttributeSet());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting users :' + error + `: ${AttributeSetController.name} -> getAttributeSet`);
		}
	}

	public async getAttributeSetById(req: Request, res: Response, next) {
		try {
			res.send(await attributeSet.getAttributeSetById(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attributeSet :' + error + `: ${AttributeSetController.name} -> getAttributeSetById`);
		}
	}

	public async updateAttributeSet(req: Request, res: Response, next) {
		try {
			res.send(await attributeSet.updateAttributeSet(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating attributeSet :' + error + `: ${AttributeSetController.name} -> updateAttributeSet`);
		}
	}

	public async deleteAttributeSet(req: Request, res: Response, next) {
		try {
			res.send(await attributeSet.deleteAttributeSet(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting attributeSet :' + error + `: ${AttributeSetController.name} -> deleteAttributeSet`);
		}
	}
}

