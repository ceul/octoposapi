
import { Request, Response } from 'express';
import { LineRemovedDAOGrowPos } from '../repository/growposDB/lineRemovedDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let lineRemoved = new LineRemovedDAOGrowPos();
export class LineRemovedController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertLineRemoved(req: Request, res: Response, next) {
		try {
			res.send(await lineRemoved.insertLineRemoved(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting lineRemoved :' + error + `: ${LineRemovedController.name} -> insertLineRemoved`);
		}
	}

	public async getLineRemoved(req: Request, res: Response, next) {
		try {
			res.send(await lineRemoved.getLineRemoved());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${LineRemovedController.name} -> getLineRemoved`);
		}
	}

	public async getLineRemovedByUser(req: Request, res: Response, next) {
		try {
			res.send(await lineRemoved.getLineRemovedByUser(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting lineRemoved :' + error + `: ${LineRemovedController.name} -> getLineRemovedByUser`);
		}
    }
    
    public async getLineRemovedByProduct(req: Request, res: Response, next) {
		try {
			res.send(await lineRemoved.getLineRemovedByProduct(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting lineRemoved :' + error + `: ${LineRemovedController.name} -> getLineRemovedByProduct`);
		}
	}
}

