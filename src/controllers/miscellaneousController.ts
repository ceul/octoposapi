
import { Request, Response } from 'express';
import { GrowPosMiscellaneousDAO } from "../repository/growposDB/miscellaneousDAO";
import { ErrorGrowPos } from '../models/growpos/error';
let product = new GrowPosMiscellaneousDAO();
export class ProductController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async getRootCatergories (req: Request, res: Response, next) {
		try {
			res.send(await product.getRootCatergories());	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while the categories were obtained: "' + error + ` ${ProductController.name}`);
		}
	}

	public async getSubCatergories (req: Request, res: Response, next) {
		try {
			res.send(await product.getSubCatergories(req.body.category));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while the categories were obtained ' + error + ` ${ProductController.name}`);
		}
	}

	public async getProductCatalog (req: Request, res: Response, next) {
		try {
			res.send(await product.getProductCatalog(req.body.category));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while the product catalog ' + error + ` ${ProductController.name}`);
		}
	}

	public async getSharedTicket (req: Request, res: Response, next) {
		try {
			res.send(await product.getSharedTicket(req.body.place));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting shared ticket: ' + error + ` ${ProductController.name}`);
		}
	}

	public async insertSharedTicket (req: Request, res: Response, next) {
		try {
			res.send(await product.insertSharedTicket(req.body));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while inserting shared ticket: ' + error + ` ${ProductController.name}`);
		}
	}

	public async updateSharedTicket (req: Request, res: Response, next) {
		try {
			res.send(await product.updateSharedTicket(req.body));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while updating shared ticket: ' + error + ` ${ProductController.name}`);
		}
	}

	public async getUser (req: Request, res: Response, next) {
		try {
			res.send(await product.getUsers());	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting user: ' + error + ` ${ProductController.name}`);
		}
	}

	public async getTaxes (req: Request, res: Response, next) {
		try {
			res.send(await product.getTaxes());	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting taxes: ' + error + ` ${ProductController.name}`);
		}
	}

	public async getFloors (req: Request, res: Response, next) {
		try {
			res.send(await product.getFloors());	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting floors: ' + error + ` ${ProductController.name}`);
		}
	}

	public async getPlaces (req: Request, res: Response, next) {
		try {
			res.send(await product.getPlaces(req.body.floor));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting places: ' + error + ` ${ProductController.name}`);
		}
	}

	public async deleteSharedTicket (req: Request, res: Response, next) {
		try {
			res.send(await product.deleteSharedTicket(req.body));	
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while deleting shared ticket: ' + error + ` ${ProductController.name}`);
		}
	}
}
    
