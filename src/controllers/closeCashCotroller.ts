
import { Request, Response } from 'express';
import { CloseCashDAOGrowPos } from '../repository/growposDB/closeCashDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let closeCash = new CloseCashDAOGrowPos();
export class CloseCashController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.insertCloseCash(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting closeCash :' + error + `: ${CloseCashController.name} -> insertCloseCash`);
		}
	}

	public async getCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.getCloseCash());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting users :' + error + `: ${CloseCashController.name} -> getCloseCash`);
		}
	}

	public async getCloseCashById(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.getCloseCashById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting closeCash :' + error + `: ${CloseCashController.name} -> getCloseCashById`);
		}
    }
    
    public async getLastCloseCashByHost(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.getLastCloseCashByHost(req.body.host));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting closeCash :' + error + `: ${CloseCashController.name} -> getLastCloseCashByHost`);
		}
	}

	public async getCloseCashByHostAndBySequence(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.getCloseCashByHostAndBySequence(req.body.host, req.body.sequence));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting closeCash by host by secuence :' + error + `: ${CloseCashController.name} -> getCloseCashByHostAndBySequence`);
		}
	}

	public async updateCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.updateCloseCash(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating closeCash :' + error + `: ${CloseCashController.name} -> updateCloseCash`);
		}
    }
    public async isCashActive(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.isCashActive(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while consulting if cash is active :' + error + `: ${CloseCashController.name} -> isCashActive`);
		}
	}

	public async moveCash(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.moveCash(req.body.closeCash, req.body.payment, res.locals.user.name ));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while moving Cash :' + error + `: ${CloseCashController.name} -> moveCash`);
		}
	}

	public async getCloseCashListByDateReport(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.getCloseCashListByDateReport(req.body.dateStart, req.body.dateEnd));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while getting clos Cash by date :' + error + `: ${CloseCashController.name} -> getCloseCashListByDateReport`);
		}
	}

	public async getCashFlowReport(req: Request, res: Response, next) {
		try {
			res.send(await closeCash.getCashFlowReport(req.body.dateStart, req.body.dateEnd));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while getting clos Cash by date :' + error + `: ${CloseCashController.name} -> getCashFlowReport`);
		}
	}
}

