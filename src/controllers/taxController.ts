
import { Request, Response } from 'express';
import { TaxDAOGrowPos } from '../repository/growposDB/taxDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let tax = new TaxDAOGrowPos();
export class TaxController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertTax(req: Request, res: Response, next) {
		try {
			res.send(await tax.insertTax(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting tax :' + error + `: ${TaxController.name} -> insertTax`);
		}
	}

	public async getTax(req: Request, res: Response, next) {
		try {
			res.send(await tax.getTax());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${TaxController.name} -> getTax`);
		}
	}

	public async getTaxById(req: Request, res: Response, next) {
		try {
			res.send(await tax.getTaxById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting tax :' + error + `: ${TaxController.name} -> getTaxById`);
		}
	}

	public async updateTax(req: Request, res: Response, next) {
		try {
			res.send(await tax.updateTax(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating tax :' + error + `: ${TaxController.name} -> updateTax`);
		}
	}

	public async deleteTax(req: Request, res: Response, next) {
		try {
			res.send(await tax.deleteTax(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting tax :' + error + `: ${TaxController.name} -> deleteTax`);
		}
	}

	public async getTaxesByCategory(req: Request, res: Response, next){
		try {
			res.send(await tax.getTaxesByCategory(req.body.category));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting taxes by category :' + error + `: ${TaxController.name} -> getTaxesByCategory`);
		}
	}

	public async getTotalTaxesByCloseCash(req: Request, res: Response, next){
		try {
			res.send(await tax.getTotalTaxesByCloseCash(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting taxes by close cash :' + error + `: ${TaxController.name} -> getTotalTaxesByCloseCash`);
		}
	}
}

