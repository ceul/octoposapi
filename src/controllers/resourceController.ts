
import { Request, Response } from 'express';
import { ErrorGrowPos } from '../models/growpos/error';
import { ResourceDAOGrowPos } from '../repository/growposDB/resourcesDAO';
import * as sharp from "sharp"
let resource = new ResourceDAOGrowPos();
export class ResourceController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertResource(req: Request, res: Response, next) {
		try {
			res.send(await resource.insertResource(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting resource :' + error + `: ${ResourceController.name} -> insertResource`);
		}
	}

	public async getResource(req: Request, res: Response, next) {
		try {
			res.send(await resource.getResource());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${ResourceController.name} -> getResource`);
		}
	}

	public async getResourceById(req: Request, res: Response, next) {
		try {
			res.send(await resource.getResourceById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting resource :' + error + `: ${ResourceController.name} -> getResourceById`);
		}
	}

	public async getResourceByHost(req: Request, res: Response, next) {
		try {
			res.send(await resource.getResourceByHost(req.body.name))
			/*let result = await resource.getResourceByHost(req.body)
			if (result.length > 0) {
				res.send(result);
			} else {
				let err: ErrorGrowPos = new Error('Resource not found');
				err.status = 404
				next(err);
			}*/
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting resource :' + error + `: ${ResourceController.name} -> getResourceByHost`);
		}
	}

	public async getCompanyLogo(req: Request, res: Response, next) {
		try {
			res.set('Content_Type', 'image/png')
			let image = await resource.getCompanyLogo()
			if(image.length < 1){
				throw new Error()
			}
			res.send(image[0].content)
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting company logo :' + error + `: ${ResourceController.name} -> getCompanyLogo`);
		}
	}

	public async updateResource(req: Request, res: Response, next) {
		try {
			res.send(await resource.updateResource(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating resource :' + error + `: ${ResourceController.name} -> updateResource`);
		}
	}

	public async updateCompanyLogo(req, res: Response, next) {
		try {
			//if you need to resize use resize({width: 250, height: 250})
			let buffer = await sharp(req.file.buffer).png().toBuffer()
			res.send(await resource.updateCompanyLogo(buffer));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating company logo :' + error + `: ${ResourceController.name} -> updateCompanyLogo`);
		}
	}

	public async uploadCompanyLogo(req, res: Response, next) {
		try {
			let buffer = await sharp(req.file.buffer).png().toBuffer()
			res.send(await resource.uploadCompanyLogo(buffer));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while uploading company logo :' + error + `: ${ResourceController.name} -> uploadCompanyLogo`);
		}
	}	

	public async deleteResource(req: Request, res: Response, next) {
		try {
			res.send(await resource.deleteResource(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting resource :' + error + `: ${ResourceController.name} -> deleteResource`);
		}
	}
}

