
import { Request, Response } from 'express';
import { UnitMeasureDAOGrowPos } from '../repository/growposDB/unitMeasureDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let unitMeasure = new UnitMeasureDAOGrowPos();
export class UnitMeasureController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertUnitMeasure(req: Request, res: Response, next) {
		try {
			res.send(await unitMeasure.insertUnitMeasure(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting unitMeasure :' + error + `: ${UnitMeasureController.name} -> insertUnitMeasure`);
		}
	}

	public async getUnitMeasure(req: Request, res: Response, next) {
		try {
			res.send(await unitMeasure.getUnitMeasure());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${UnitMeasureController.name} -> getUnitMeasure`);
		}
	}

	public async getUnitMeasureById(req: Request, res: Response, next) {
		try {
			res.send(await unitMeasure.getUnitMeasureById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting unitMeasure :' + error + `: ${UnitMeasureController.name} -> getUnitMeasureById`);
		}
	}

	public async updateUnitMeasure(req: Request, res: Response, next) {
		try {
			res.send(await unitMeasure.updateUnitMeasure(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating unitMeasure :' + error + `: ${UnitMeasureController.name} -> updateUnitMeasure`);
		}
	}

	public async deleteUnitMeasure(req: Request, res: Response, next) {
		try {
			res.send(await unitMeasure.deleteUnitMeasure(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting unitMeasure :' + error + `: ${UnitMeasureController.name} -> deleteUnitMeasure`);
		}
	}
}

