
import { Request, Response } from 'express';
import { TaxCategoryDAOGrowPos } from '../repository/growposDB/taxCategoryDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let taxCategory = new TaxCategoryDAOGrowPos();
export class TaxCategoryController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertTaxCategory(req: Request, res: Response, next) {
		try {
			res.send(await taxCategory.insertTaxCategory(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting taxCategory :' + error + `: ${TaxCategoryController.name} -> insertTaxCategory`);
		}
	}

	public async getTaxCategory(req: Request, res: Response, next) {
		try {
			res.send(await taxCategory.getTaxCategory());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${TaxCategoryController.name} -> getTaxCategory`);
		}
	}

	public async getTaxCategoryById(req: Request, res: Response, next) {
		try {
			res.send(await taxCategory.getTaxCategoryById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting taxCategory :' + error + `: ${TaxCategoryController.name} -> getTaxCategoryById`);
		}
	}

	public async updateTaxCategory(req: Request, res: Response, next) {
		try {
			res.send(await taxCategory.updateTaxCategory(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating taxCategory :' + error + `: ${TaxCategoryController.name} -> updateTaxCategory`);
		}
	}

	public async deleteTaxCategory(req: Request, res: Response, next) {
		try {
			res.send(await taxCategory.deleteTaxCategory(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting taxCategory :' + error + `: ${TaxCategoryController.name} -> deleteTaxCategory`);
		}
	}

	public async getTotalTaxesByCloseCash(req: Request, res: Response, next) {
		try {
			res.send(await taxCategory.getTotalTaxesByCloseCash(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting taxes by clos cash :' + error + `: ${TaxCategoryController.name} -> getTotalTaxesByCloseCash`);
		}
	}
}

