
import { Request, Response } from 'express';
import { VehicleDAOGrowPos } from '../repository/growposDB/vehicleDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let vehicle = new VehicleDAOGrowPos();
export class VehicleController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertVehicle(req: Request, res: Response, next) {
		try {
			res.send(await vehicle.insertVehicle(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting vehicle :' + error + `: ${VehicleController.name} -> insertVehicle`);
		}
	}

	public async getVehicle(req: Request, res: Response, next) {
		try {
			res.send(await vehicle.getVehicle());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${VehicleController.name} -> getVehicle`);
		}
	}

	public async getVehicleById(req: Request, res: Response, next) {
		try {
			res.send(await vehicle.getVehicleById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting vehicle :' + error + `: ${VehicleController.name} -> getVehicleById`);
		}
	}

	public async getVehicleByPlate(req: Request, res: Response, next) {
		try {
			res.send(await vehicle.getVehicleByPlate(req.body.plate));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting vehicle by plate :' + error + `: ${VehicleController.name} -> getVehicleByPlate`);
		}
	}

	public async updateVehicle(req: Request, res: Response, next) {
		try {
			res.send(await vehicle.updateVehicle(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating vehicle :' + error + `: ${VehicleController.name} -> updateVehicle`);
		}
	}

	public async deleteVehicle(req: Request, res: Response, next) {
		try {
			res.send(await vehicle.deleteVehicle(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting vehicle :' + error + `: ${VehicleController.name} -> deleteVehicle`);
		}
	}
}

