
import { Request, Response } from 'express';
import { SupplierDAOGrowPos } from '../repository/growposDB/supplierDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let supplier = new SupplierDAOGrowPos();
export class SupplierController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertSupplier(req: Request, res: Response, next) {
		try {
			res.send(await supplier.insertSupplier(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting supplier :' + error + `: ${SupplierController.name} -> insertSupplier`);
		}
	}

	public async getSupplier(req: Request, res: Response, next) {
		try {
			res.send(await supplier.getSupplier());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting suppliers :' + error + `: ${SupplierController.name} -> getSupplier`);
		}
	}

	public async getVisibleSupplier(req: Request, res: Response, next) {
		try {
			res.send(await supplier.getVisibleSupplier());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting visible suppliers :' + error + `: ${SupplierController.name} -> getVisibleSupplier`);
		}
	}

	public async getSupplierById(req: Request, res: Response, next) {
		try {
			res.send(await supplier.getSupplierById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting supplier :' + error + `: ${SupplierController.name} -> getSupplierById`);
		}
	}

	public async updateSupplier(req: Request, res: Response, next) {
		try {
			res.send(await supplier.updateSupplier(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating supplier :' + error + `: ${SupplierController.name} -> updateSupplier`);
		}
	}

	public async deleteSupplier(req: Request, res: Response, next) {
		try {
			res.send(await supplier.deleteSupplier(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting supplier :' + error + `: ${SupplierController.name} -> deleteSupplier`);
		}
	}
}

