
import { Request, Response } from 'express';
import { ContractDAOGrowPos } from '../repository/growposDB/contractDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let contract = new ContractDAOGrowPos();
export class ContractController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertContract(req: Request, res: Response, next) {
		try {
			res.send(await contract.insertContract(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting contract :' + error + `: ${ContractController.name} -> insertContract`);
		}
	}

	public async getContract(req: Request, res: Response, next) {
		try {
			res.send(await contract.getContract());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${ContractController.name} -> getContract`);
		}
	}

	public async getContractById(req: Request, res: Response, next) {
		try {
			res.send(await contract.getContractById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting contract :' + error + `: ${ContractController.name} -> getContractById`);
		}
	}

	public async getContractByPlate(req: Request, res: Response, next) {
		try {
			res.send(await contract.getContractByPlate(req.body.plate));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting contract :' + error + `: ${ContractController.name} -> getContractByPlate`);
		}
	}

	public async updateContract(req: Request, res: Response, next) {
		try {
			res.send(await contract.updateContract(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating contract :' + error + `: ${ContractController.name} -> updateContract`);
		}
	}

	public async deleteContract(req: Request, res: Response, next) {
		try {
			res.send(await contract.deleteContract(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting contract :' + error + `: ${ContractController.name} -> deleteContract`);
		}
	}
}

