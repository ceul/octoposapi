
import { Request, Response } from 'express';
import { FloorDAOGrowPos } from '../repository/growposDB/floorDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let floor = new FloorDAOGrowPos();
export class FloorController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertFloor(req: Request, res: Response, next) {
		try {
			res.send(await floor.insertFloor(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting floor :' + error + `: ${FloorController.name} -> insertFloor`);
		}
	}

	public async getFloor(req: Request, res: Response, next) {
		try {
			res.send(await floor.getFloor(req.body.type));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting floor :' + error + `: ${FloorController.name} -> getFloor`);
		}
	}

	public async getFloorById(req: Request, res: Response, next) {
		try {
			res.send(await floor.getFloorById(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting floor :' + error + `: ${FloorController.name} -> getFloorById`);
		}
    }
    
    public async getFloorsWithPlace(req: Request, res: Response, next) {
		try {
			res.send(await floor.getFloorsWithPlace(req.body.type));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting floor with places :' + error + `: ${FloorController.name} -> getFloorsWithPlace`);
		}
    }

	public async updateFloor(req: Request, res: Response, next) {
		try {
			res.send(await floor.updateFloor(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating floor :' + error + `: ${FloorController.name} -> updateFloor`);
		}
	}

	public async deleteFloor(req: Request, res: Response, next) {
		try {
			res.send(await floor.deleteFloor(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting floor :' + error + `: ${FloorController.name} -> deleteFloor`);
		}
	}
}

