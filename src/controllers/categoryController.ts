
import { Request, Response } from 'express';
import { CategoryDAOGrowPos } from '../repository/growposDB/categoryDAO';
import { LogDAOGrowPos } from '../repository/growposDB/logDAO';
import { LogEnum } from '../models/growpos/log.enum';
import { ErrorGrowPos } from '../models/growpos/error';
let category = new CategoryDAOGrowPos()
let log =  new LogDAOGrowPos() 
import * as sharp from "sharp"

export class CategoryController {

	/*-------------------------------- app --------------------------------------------------------*/
	public async insertCategory(req: Request, res: Response, next) {
		try {
			res.send(await category.insertCategory(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> insertCategory: ${error}`)
		}
	}

	public async getCategories(req: Request, res: Response, next) {
		try {
			res.send(await category.getCategories());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getCategories: ${error}`)
		}
	}

	public async getParkingCategories(req: Request, res: Response, next) {
		try {
			res.send(await category.getParkingCategories());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getParkingCategories: ${error}`)
		}
	}
	
	public async getRootCategories(req: Request, res: Response, next) {
		try {
			res.send(await category.getRootCategories());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getRootCategories: ${error}`)
		}
	}

	public async getChildCategories(req: Request, res: Response, next) {
		try {
			res.send(await category.getChildCategories(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getChildCategories: ${error}`)
		}
	}

	public async getCategoryById(req: Request, res: Response, next) {
		try {
			res.send(await category.getCategoryById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getCategoryById: ${error}`)
		}
	}

	public async getCategorySalesReport(req: Request, res: Response, next) {
		try {
			res.send(await category.getCategorySalesReport(req.body.datestart, req.body.dateend));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getCategorySalesReport: ${error}`)
		}
	}

	public async getCategoryImage(req: Request, res: Response, next) {
		try {
			res.set('Content_Type', 'image/png')
			let image = await category.getCategoryImage(req.params.id)
			if(image[0].image === null || image[0].image === undefined){
				throw new Error()
			}
			res.send(image[0].image)
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting category image:' + error + `: ${CategoryController.name} -> getCategoryImage`);
		}
	}

	public async updateCategory(req: Request, res: Response, next) {
		try {
			res.send(await category.updateCategory(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> updateCategory: ${error}`)
		}
	}

	public async uploadPicture(req, res: Response, next) {
		try {
			let buffer = await sharp(req.file.buffer).png().toBuffer()
			res.send(await category.uploadPicture(req.params.id, buffer));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while uploding category picture :' + error + `: ${CategoryController.name} -> uploadPicture`);
		}
	}

	public async deleteCategory(req: Request, res: Response, next) {
		try {
			res.send(await category.deleteCategory(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> deleteCategory: ${error}`)
		}
	}

	public async getCategoryBySales(req: Request, res: Response, next) {
		try {
			res.send(await category.getCategoryBySales());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			log.insertLog(LogEnum.ERROR, `${CategoryController.name} -> getCategoryBySales: ${error}`)
		}
	}
}

