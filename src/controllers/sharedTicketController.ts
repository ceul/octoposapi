
import { Request, Response } from 'express';
import { SharedTicketDAOGrowPos } from '../repository/growposDB/sharedTicketDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let sharedTicket = new SharedTicketDAOGrowPos();
export class SharedTicketController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertSharedTicket(req: Request, res: Response, next) {
		try {
			res.send(await sharedTicket.insertSharedTicket(req.body.id, req.body.ticket));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while inserting sharedTicket :' + error + `: ${SharedTicketController.name} -> insertSharedTicket`);
		}
	}

	public async getSharedTicketList(req: Request, res: Response, next) {
		try {
			res.send(await sharedTicket.getSharedTicketList());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${SharedTicketController.name} -> getSharedTicketById`);
		}
	}

	public async getSharedTicketById(req: Request, res: Response, next) {
		try {
			res.send(await sharedTicket.getSharedTicketById(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while getting users :' + error + `: ${SharedTicketController.name} -> getSharedTicketById`);
		}
	}

	public async updateSharedTicket(req: Request, res: Response, next) {
		try {
			res.send(await sharedTicket.updateSharedTicket(req.body.id, req.body.ticket));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 400
			next(err);
			console.log('An error occurred while updating sharedTicket :' + error + `: ${SharedTicketController.name} -> updateSharedTicket`);
		}
	}

	public async deleteSharedTicket(req: Request, res: Response, next) {
		try {
			res.send(await sharedTicket.deleteSharedTicket(req.body.id));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while deleting sharedTicket :' + error + `: ${SharedTicketController.name} -> deleteSharedTicket`);
		}
	}
}

