
import { Request, Response } from 'express';
import { AttributeDAOGrowPos } from '../repository/growposDB/attributeDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let attribute = new AttributeDAOGrowPos();
export class AttributeController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertAttribute(req: Request, res: Response, next) {
		try {
			res.send(await attribute.insertAttribute(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting attribute :' + error + `: ${AttributeController.name} -> insertAttribute`);
		}
	}

	public async getAttribute(req: Request, res: Response, next) {
		try {
			res.send(await attribute.getAttribute());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting users :' + error + `: ${AttributeController.name} -> getAttribute`);
		}
	}

	public async getAttributeById(req: Request, res: Response, next) {
		try {
			res.send(await attribute.getAttributeById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attribute :' + error + `: ${AttributeController.name} -> getAttributeById`);
		}
	}


	public async getAttributeByAttributeSet(req: Request, res: Response, next) {
		try {
			res.send(await attribute.getAttributeByAttributeSet(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attributes by attribute set:' + error + `: ${AttributeController.name} -> getAttributeByAttributeSet`);
		}
	}

	public async updateAttribute(req: Request, res: Response, next) {
		try {
			res.send(await attribute.updateAttribute(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating attribute :' + error + `: ${AttributeController.name} -> updateAttribute`);
		}
	}

	public async deleteAttribute(req: Request, res: Response, next) {
		try {
			res.send(await attribute.deleteAttribute(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting attribute :' + error + `: ${AttributeController.name} -> deleteAttribute`);
		}
	}
}

