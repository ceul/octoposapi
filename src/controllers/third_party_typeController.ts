//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { ThirdPartyTypesDAOGrowAccounting } from '../repository/growposDB/third_party_typeDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let obj = new ThirdPartyTypesDAOGrowAccounting();
export class ThirdPartyTypesController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertThirdPartyType(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertThirdPartyType(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting third party type :' + error + `: ${ThirdPartyTypesController.name} -> insertThirdPartyType`);
		}
	}

	public async getThirdPartyTypes(req: Request, res: Response, next) {
		try {
			res.send(await obj.getThirdPartyTypes());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting third party type :' + error + `: ${ThirdPartyTypesController.name} -> getThirdPartyTypes`);
		}
	}

	public async getThirdPartyTypeById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getThirdPartyTypeById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting third party type :' + error + `: ${ThirdPartyTypesController.name} -> getThirdPartyTypeById`);
		}
	}

	public async updateThirdPartyType(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateThirdPartyType(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating third party type :' + error + `: ${ThirdPartyTypesController.name} -> updateThirdPartyType`);
		}
	}

	public async deleteThirdPartyType(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteThirdPartyType(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting third party type :' + error + `: ${ThirdPartyTypesController.name} -> deleteThirdPartyType`);
		}
	}
}