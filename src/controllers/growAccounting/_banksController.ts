//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { BanksDAOGrowAccounting } from '../../repository/growaccountingDB/_banksDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new BanksDAOGrowAccounting();
export class BanksController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertBank(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertBank(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting banks :' + error + `: ${BanksController.name} -> ${this.insertBank.name}`);
		}
	}

	public async getBanks(req: Request, res: Response, next) {
		try {
			res.send(await obj.getBanks());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting banks :' + error + `: ${BanksController.name} -> ${this.getBanks.name}`);
		}
	}

	public async getBanksById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getBanksById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting banks :' + error + `: ${BanksController.name} -> ${this.getBanksById.name}`);
		}
	}

	public async updateBank(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateBank(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating banks :' + error + `: ${BanksController.name} -> ${this.updateBank.name}`);
		}
	}

	public async deleteBank(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteBank(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting banks :' + error + `: ${BanksController.name} -> ${this.deleteBank.name}`);
		}
	}
}