//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { CostCentersDAOGrowAccounting } from '../../repository/growaccountingDB/_cost_centersDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new CostCentersDAOGrowAccounting();
export class CostCentersController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertCostCenter(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertCostCenter(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting cost center :' + error + `: ${CostCentersController.name} -> ${this.insertCostCenter.name}`);
		}
	}

	public async getCostCenters(req: Request, res: Response, next) {
		try {
			res.send(await obj.getCostCenters());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting cost center :' + error + `: ${CostCentersController.name} -> ${this.getCostCenters.name}`);
		}
	}

	public async getCostCentersById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getCostCentersById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting cost center :' + error + `: ${CostCentersController.name} -> ${this.getCostCentersById.name}`);
		}
	}

	public async updateCostCenter(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateCostCenter(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating cost center :' + error + `: ${CostCentersController.name} -> ${this.updateCostCenter.name}`);
		}
	}

	public async deleteCostCenter(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteCostCenter(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting cost center :' + error + `: ${CostCentersController.name} -> ${this.deleteCostCenter.name}`);
		}
	}
}