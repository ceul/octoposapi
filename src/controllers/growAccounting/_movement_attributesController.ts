//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { MovementsAttributesDAOGrowAccounting } from '../../repository/growaccountingDB/_movement_attributesDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new MovementsAttributesDAOGrowAccounting();
export class MovementsAttributesController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertMovementsAttributes(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertMovementsAttributes(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting movement attributes :' + error + `: ${MovementsAttributesController.name} -> ${this.insertMovementsAttributes.name}`);
		}
	}

	public async getAMovementsAttributes(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAMovementsAttributes());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting movement attributes :' + error + `: ${MovementsAttributesController.name} -> ${this.getAMovementsAttributes.name}`);
		}
	}

	public async getMovementsAttributesById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getMovementsAttributesById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting movement attributes :' + error + `: ${MovementsAttributesController.name} -> ${this.getMovementsAttributesById.name}`);
		}
	}

	public async deleteMovementsAttributes(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteMovementsAttributes(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting movement attributes :' + error + `: ${MovementsAttributesController.name} -> ${this.deleteMovementsAttributes.name}`);
		}
	}
}