//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { MovementsDAOGrowAccounting } from '../../repository/growaccountingDB/_movementsDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new MovementsDAOGrowAccounting();
export class MovementsController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertMovement(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertMovement(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting movements :' + error + `: ${MovementsController.name} -> ${this.insertMovement.name}`);
		}
	}

	public async getMovements(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAMovements());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting movements :' + error + `: ${MovementsController.name} -> ${this.getMovements.name}`);
		}
	}

	public async getMovementsById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getMovementsById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting movements :' + error + `: ${MovementsController.name} -> ${this.getMovementsById.name}`);
		}
	}

	public async inactiveMovement(req: Request, res: Response, next) {
		try {
			res.send(await obj.inactiveMovement(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating movements :' + error + `: ${MovementsController.name} -> ${this.inactiveMovement.name}`);
		}
	}
}