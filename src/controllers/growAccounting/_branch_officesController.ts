//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { BranchOfficesDAOGrowAccounting } from '../../repository/growaccountingDB/_branch_officesDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new BranchOfficesDAOGrowAccounting();
export class BranchOfficesController {


	/*-------------------------------- app --------------------------------------------------------*/
	public async insertBranchOffice(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertBranchOffice(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting branch office :' + error + `: ${BranchOfficesController.name} -> ${this.insertBranchOffice.name}`);
		}
	}

	public async getBranchOffices(req: Request, res: Response, next) {
		try {
			res.send(await obj.getBranchOffices());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting branch office :' + error + `: ${BranchOfficesController.name} -> ${this.getBranchOffices.name}`);
		}
	}

	public async getBranchOfficeById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getBranchOfficesById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting branch office :' + error + `: ${BranchOfficesController.name} -> ${this.getBranchOfficeById.name}`);
		}
	}

	public async updateBranchOffice(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateBranchOffice(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating branch office :' + error + `: ${BranchOfficesController.name} -> ${this.updateBranchOffice.name}`);
		}
	}

	public async deleteBranchOffices(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteBranchOffice(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting branch office :' + error + `: ${BranchOfficesController.name} -> ${this.deleteBranchOffices.name}`);
		}
	}
}

