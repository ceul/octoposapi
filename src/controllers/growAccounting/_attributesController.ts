//    Grow
//    Copyright (c) 2020 GrowAccounting
//    http://growpos.co
//
//    This file is part of GrowPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import { Request, Response } from 'express';
import { AttributesDAOGrowAccounting } from '../../repository/growaccountingDB/_attributeDAO';
import { ErrorGrowPos } from '../../models/growpos/error';
let obj = new AttributesDAOGrowAccounting();
export class AttributesController {

	/*------------------------------------- app --------------------------------------------------------*/
	public async insertAttribute(req: Request, res: Response, next) {
		try {
			res.send(await obj.insertAttribute(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while inserting attributes :' + error + `: ${AttributesController.name} -> ${this.insertAttribute.name}`);
		}
	}

	public async getAttributes(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAttributes());
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attributes :' + error + `: ${AttributesController.name} -> ${this.getAttributes.name}`);
		}
	}

	public async getAttributesById(req: Request, res: Response, next) {
		try {
			res.send(await obj.getAttributesById(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 404
            next(err);
			console.log('An error occurred while getting attributes :' + error + `: ${AttributesController.name} -> ${this.getAttributesById.name}`);
		}
	}

	public async updateAttribute(req: Request, res: Response, next) {
		try {
			res.send(await obj.updateAttribute(req.body));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 400
            next(err);
			console.log('An error occurred while updating attributes :' + error + `: ${AttributesController.name} -> ${this.updateAttribute.name}`);
		}
	}

	public async deleteAttribute(req: Request, res: Response, next) {
		try {
			res.send(await obj.deleteAttribute(req.body.id));
		} catch (error) {
			let err : ErrorGrowPos = new Error(error);
            err.status = 500
            next(err);
			console.log('An error occurred while deleting attributes :' + error + `: ${AttributesController.name} -> ${this.deleteAttribute.name}`);
		}
	}
}