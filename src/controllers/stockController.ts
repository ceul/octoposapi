
import { Request, Response } from 'express';
import { StockDAOGrowPos } from '../repository/growposDB/stockDAO';
import { ErrorGrowPos } from '../models/growpos/error';
let stock = new StockDAOGrowPos();
export class StockController {


    /*-------------------------------- app --------------------------------------------------------*/
    
	public async insertStockDiary(req: Request, res: Response, next) {
		try {
			res.status(201).send(await stock.insertStockDiary(req.body));
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 500
			next(err);
			console.log('An error occurred while inserting stock diary :' + error + `: ${StockController.name} -> insertStockDiary`);
		}
	}

	public async getStock(req: Request, res: Response, next) {
		try {
			res.send(await stock.getStock());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while gettin current stock :' + error + `: ${StockController.name} -> getStock`);
		}
	}

	public async getLowStock(req: Request, res: Response, next) {
		try {
			res.send(await stock.getLowStock());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while gettin low stock :' + error + `: ${StockController.name} -> getLowStock`);
		}
	}

	public async getLocationStock(req: Request, res: Response, next) {
		try {
			res.send(await stock.getLocationStock());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while gettin low stock :' + error + `: ${StockController.name} -> getLocationStock`);
		}
	}

	public async refactorStockCurrent(req: Request, res: Response, next) {
		try {
			res.send(await stock.refactorStockCurrent());
		} catch (error) {
			let err: ErrorGrowPos = new Error(error);
			err.status = 404
			next(err);
			console.log('An error occurred while refactor stockcurrent :' + error + `: ${StockController.name} -> refactorStockCurrent`);
		}
	}

	
}