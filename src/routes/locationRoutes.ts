
import {Request, Response, NextFunction} from "express";
import { LocationController } from "../controllers/locationController";
import * as auth from '../authService'
var cors = require('cors');
export class LocationRoutes { 
    
    public locationController: LocationController = new LocationController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/location')
        .post(auth,this.locationController.insertLocation)

        app.route('/location/get')
        .post(auth,this.locationController.getLocation)

        app.route('/location/getById')
        .post(auth,this.locationController.getLocationById)

        app.route('/location/update')
        .post(auth,this.locationController.updateLocation)

        app.route('/location/delete')
        .post(auth,this.locationController.deleteLocation)
        
    }
    
}