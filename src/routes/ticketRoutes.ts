
import {Request, Response, NextFunction} from "express";
import { TicketController } from "../controllers/ticketController";
import * as auth from '../authService'
var cors = require('cors');
export class TicketRoutes { 
    
    public ticketController: TicketController = new TicketController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/ticket')
        .post(auth,this.ticketController.saveTicket);

        app.route('/ticket/ts')
        .post(auth,this.ticketController.getTotalSales);
        
        app.route('/ticket/pr')
        .post(auth,this.ticketController.getProfit);

        app.route('/ticket/nt')
        .post(auth,this.ticketController.getNumTicket);

        app.route('/ticket/ds')
        .post(auth,this.ticketController.getDailySales);

        app.route('/ticket/getTotalByCloseCash')
        .post(auth,this.ticketController.getTotalSalesByCloseCash);

        app.route('/ticket/getReceiptFiltered')
        .post(auth,this.ticketController.getReceiptFiltered);

        app.route('/ticket/getReceiptById')
        .post(auth,this.ticketController.getReceiptById);
        
    }
    
}