
import {Request, Response, NextFunction} from "express";
import { DrawerOpenedController } from "../controllers/drawerOpenedController";
import * as auth from '../authService'
var cors = require('cors');
export class DrawerOpenedRoutes { 
    
    public drawerOpenedController: DrawerOpenedController = new DrawerOpenedController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/drawer-opened')
        .post(auth,this.drawerOpenedController.insertDrawerOpened)

        app.route('/drawer-opened/get')
        .post(auth,this.drawerOpenedController.getDrawerOpened)

        app.route('/drawer-opened/getByUser')
        .post(auth,this.drawerOpenedController.getDrawerOpenedByUser)

        app.route('/drawer-opened/getByTicket')
        .post(auth,this.drawerOpenedController.getDrawerOpenedByTicket)
    }
    
}