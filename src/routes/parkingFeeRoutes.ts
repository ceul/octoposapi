
import {Request, Response, NextFunction} from "express";
import * as auth from '../authService'
import { ParkingFeeController } from "../controllers/parkingFeeController";
var cors = require('cors');
export class ParkingFeeRoutes { 
    
    public parkingFeeController: ParkingFeeController = new ParkingFeeController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/parking-fee')
        .post(auth,this.parkingFeeController.insertParkingFee)

        app.route('/parking-fee/get')
        .post(auth,this.parkingFeeController.getParkingFee)

        app.route('/parking-fee/getGroupByType')
        .post(auth,this.parkingFeeController.getParkingFeeGroupByType)

        app.route('/parking-fee/getById')
        .post(auth,this.parkingFeeController.getParkingFeeById)

        app.route('/parking-fee/getByType')
        .post(auth,this.parkingFeeController.getParkingFeeByType)

        app.route('/parking-fee/update')
        .post(auth,this.parkingFeeController.updateParkingFee)

        app.route('/parking-fee/delete')
        .post(auth,this.parkingFeeController.deleteParkingFee)
        
    }
    
}