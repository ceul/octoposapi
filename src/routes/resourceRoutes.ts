
import {Request, Response, NextFunction} from "express";
import * as auth from '../authService'
import { ResourceController } from "../controllers/resourceController";
import * as multer from "multer"
var cors = require('cors');
export class ResourceRoutes { 

    upload = multer({
        limits: {
            fileSize: 1000000
        },
        fileFilter(req, file, cb) {
            if(!file.originalname.match(/\.(jpg|jpeg|png)$/)){
                return cb(new Error('File must be a Image'))
            }

            cb(undefined, true)
        }
    })
    
    public resourceController: ResourceController = new ResourceController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/resource')
        .post(auth,this.resourceController.insertResource)

        app.route('/resource/get')
        .post(auth,this.resourceController.getResource)

        app.route('/resource/getById')
        .post(auth,this.resourceController.getResourceById)

        app.route('/resource/getByHost')
        .post(auth,this.resourceController.getResourceByHost)

        // OJOOOOO NO TIENE AUTENTICACION
        app.route('/resource/getCompanyLogo')
        .get(this.resourceController.getCompanyLogo)

        app.route('/resource/update')
        .post(auth,this.resourceController.updateResource)

        app.route('/resource/updateCompanyLogo')
        .post(auth,this.upload.single('upload'),this.resourceController.updateCompanyLogo)

        app.route('/resource/uploadCompanyLogo')
        .post(this.upload.single('upload'),this.resourceController.uploadCompanyLogo)

        app.route('/resource/delete')
        .post(auth,this.resourceController.deleteResource)
        
    }
    
}