//    StarPos
//    Copyright (c) 2018 StarPos
//    http://starpos.co
//
//    This file is part of StarPos
//
//    StarPos is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   StarPos is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with StarPos.  If not, see <http://www.gnu.org/licenses/>.

import {Request, Response, NextFunction} from "express";
import { AttributesController } from "../../controllers/growAccounting/_attributesController";
import * as auth from '../../authService'
import * as cors from 'cors'
export class AttributesRoutes { 
    
    public objController: AttributesController = new AttributesController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/_attributes')
        .post(auth,this.objController.insertAttribute)

        app.route('/_attributes/get')
        .post(auth,this.objController.getAttributes)

        app.route('/_attributes/getById')
        .post(auth,this.objController.getAttributesById)

        app.route('/_attributes/update')
        .post(auth,this.objController.updateAttribute)

        app.route('/_attributes/delete')
        .post(auth,this.objController.deleteAttribute)
        
    }
    
}