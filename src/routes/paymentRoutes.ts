
import { Request, Response, NextFunction } from "express";
import { PaymentController } from "../controllers/paymentController";
import * as auth from '../authService'
var cors = require('cors');
export class PaymentRoutes {

    public paymentController: PaymentController = new PaymentController();

    public routes(app): void {
        app.use(cors());

        app.route('/payment/getByCloseCash')
            .post(auth, this.paymentController.getByCloseCash)

        app.route('/payment/getTotalPaymentsByCloseCash')
            .post(auth, this.paymentController.getTotalPaymentsByCloseCash)

        app.route('/payment/getTransactionsReport')
            .post(auth, this.paymentController.getTransactionsReport)

        app.route('/payment/getTransactionsReportByPayment')
            .post(auth, this.paymentController.getTransactionsReportByPayment)

    }

}