
import {Request, Response, NextFunction} from "express";
import { LineRemovedController } from "../controllers/lineRemovedController";
import * as auth from '../authService'
var cors = require('cors');
export class LineRemovedRoutes { 
    
    public lineRemovedController: LineRemovedController = new LineRemovedController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/line-removed')
        .post(auth,this.lineRemovedController.insertLineRemoved)

        app.route('/line-removed/get')
        .post(auth,this.lineRemovedController.getLineRemoved)

        app.route('/line-removed/getByUser')
        .post(auth,this.lineRemovedController.getLineRemovedByUser)

        app.route('/line-removed/getByProduct')
        .post(auth,this.lineRemovedController.getLineRemovedByProduct)
    }
    
}