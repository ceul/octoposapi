
import {Request, Response, NextFunction} from "express";
import { CustomerController } from "../controllers/customerController";
import * as auth from '../authService'
var cors = require('cors');
export class CustomerRoutes { 
    
    public customerController: CustomerController = new CustomerController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/customer')
        .post(auth,this.customerController.insertCustomer)

        app.route('/customer/get')
        .post(auth,this.customerController.getCustomers)

        app.route('/customer/getById')
        .post(auth,this.customerController.getCustomerById)

        app.route('/customer/getFiltered')
        .post(auth,this.customerController.getCustomerFiltered)

        app.route('/customer/getTransactions')
        .post(auth,this.customerController.getTransactions)

        app.route('/customer/update')
        .post(auth,this.customerController.updateCustomer)

        app.route('/customer/delete')
        .post(auth,this.customerController.deleteCustomer)

        
        
    }
    
}