
import {Request, Response, NextFunction} from "express";
import { RoleController } from "../controllers/roleController";
import * as auth from '../authService'
var cors = require('cors');
export class RoleRoutes { 
    
    public roleController: RoleController = new RoleController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/role')
        .post(auth,this.roleController.insertRole)

        app.route('/role/get')
        .post(auth,this.roleController.getRole)

        app.route('/role/getById')
        .post(auth,this.roleController.getRoleById)

        app.route('/role/update')
        .post(auth,this.roleController.updateRole)

        app.route('/role/delete')
        .post(auth,this.roleController.deleteRole)
        
    }
    
}