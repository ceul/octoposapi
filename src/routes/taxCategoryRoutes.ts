
import {Request, Response, NextFunction} from "express";
import { TaxCategoryController } from "../controllers/taxCategoryController";
import * as auth from '../authService'
var cors = require('cors');
export class TaxCategoryRoutes { 
    
    public taxCategoryController: TaxCategoryController = new TaxCategoryController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/tax-category')
        .post(auth,this.taxCategoryController.insertTaxCategory)

        app.route('/tax-category/get')
        .post(auth,this.taxCategoryController.getTaxCategory)

        app.route('/tax-category/getById')
        .post(auth,this.taxCategoryController.getTaxCategoryById)

        app.route('/tax-category/update')
        .post(auth,this.taxCategoryController.updateTaxCategory)

        app.route('/tax-category/delete')
        .post(auth,this.taxCategoryController.deleteTaxCategory)

        app.route('/tax-category/getTotalByCloseCash')
        .post(auth,this.taxCategoryController.getTotalTaxesByCloseCash)
        
    }
    
}