
import {Request, Response, NextFunction} from "express";
import { SupplierController } from "../controllers/supplierController";
import * as auth from '../authService'
var cors = require('cors');
export class SupplierRoutes { 
    
    public supplierController: SupplierController = new SupplierController();

    public routes(app): void {   
        
        app.use(cors());

        app.route('/supplier')
        .post(auth,this.supplierController.insertSupplier)

        app.route('/supplier/get')
        .post(auth,this.supplierController.getSupplier)

        app.route('/supplier/getVisible')
        .post(auth,this.supplierController.getVisibleSupplier)

        app.route('/supplier/getById')
        .post(auth,this.supplierController.getSupplierById)

        app.route('/supplier/update')
        .post(auth,this.supplierController.updateSupplier)

        app.route('/supplier/delete')
        .post(auth,this.supplierController.deleteSupplier)
        
    }
    
}